<?php

namespace App;

use Nette;
use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\Route;


class RouterFactory
{

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
		$router = new RouteList;
                $router[] = new Route('login', 'Homepage:login');
                $router[] = new Route('agenda', 'Homepage:agenda');
                $router[] = new Route('<presenter>/<action>/<day>/<month>/<year>', 'Calendar:day');
                $router[] = new Route('<presenter>/<action>/<id>/<name>', 'Admin:changeCategory');
		$router[] = new Route('<presenter>/<action>[/<id>]', 'Homepage:default');
		return $router;
	}

}
