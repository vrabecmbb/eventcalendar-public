<?php


namespace App\Model;

class VehiclesManager{
    
    public $em;

    public function __construct(\Kdyby\Doctrine\Registry $registry) {
        $this->em = $registry->getManager('noetic');
    }
    
    public function getVehiclesList(){
        $qb = $this->em->createQueryBuilder();

        $qb->select('v')
                ->from(\App\Vehicles::class, 'v')
                ->orderBy('v.id', 'ASC')
        ;

        $noetics = $qb->getQuery()->getResult();

        return $noetics;
    }
    
    public function getVehicle($id){
        return $this->em->getRepository(\App\Vehicles::class)->find($id);
    }
}