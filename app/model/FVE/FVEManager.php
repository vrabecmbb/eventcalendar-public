<?php

namespace App\Model;

class FVEManager {

    public $em;

    public function __construct(\Kdyby\Doctrine\Registry $registry) {
        $this->em = $registry->getManager('noetic');
    }

    public function getFVEList() {

        $qb = $this->em->createQueryBuilder();

        $qb->select('f')
                ->from(\App\FVE::class, 'f')
                ->orderBy('f.name', 'ASC')
        ;

        $noetics = $qb->getQuery()->getResult();

        return $noetics;
    }
    
    public function getById($id){
        return $this->em->getRepository(\App\FVE::class)->find($id);
    }

}
