<?php

namespace App\Model;

class NoeticManager {

    public $em;

    public function __construct(\Kdyby\Doctrine\Registry $registry) {
        $this->em = $registry->getManager('noetic');
    }

    public function getNoeticsList() {

        $qb = $this->em->createQueryBuilder();

        $qb->select('n')
                ->from(\App\Noetics::class, 'n')
                ->orderBy('n.scada_name', 'ASC')
        ;

        $noetics = $qb->getQuery()->getResult();

        return $noetics;
    }
    
    public function getById($id){
        return $this->em->getRepository(\App\Noetics::class)->find($id);
    }

}
