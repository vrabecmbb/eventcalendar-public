<?php

namespace App\Model;

class BuildingsManager {

    public $em;

    public function __construct(\Kdyby\Doctrine\Registry $registry) {
        $this->em = $registry->getManager('noetic');
    }

    /**
     * Buildings list
     * @return type
     */
    public function getBuildingsList() {

        $qb = $this->em->createQueryBuilder();

        $qb->select('b')
                ->from(\App\Buildings::class, 'b')
                ->orderBy('b.name', 'ASC')
        ;

        $noetics = $qb->getQuery()->getResult();

        return $noetics;
    }
    
    /**
     * Find building by id
     * @param type $id
     * @return type
     */
    public function getBuilding($id){
        return $this->em->getRepository(\App\Buildings::class)->find($id);
    }

}
