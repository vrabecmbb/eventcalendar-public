<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use Kdyby\Doctrine\Registry;

class Authenticator extends Nette\Object implements Nette\Security\IAuthenticator {
    
    /** @var \Doctrine\ORM\EntityRepository */
    private $users;
    
    public function __construct(Registry $registry)
    {
        $this->users = $registry->getManager('noetic')->getRepository(\App\Users::class);
    }
    
    public function authenticate(array $credentials)
    {
        $bcrypt = new Libs\Bcrypt();
        
        list($username) = $credentials;
	$user = $this->users->findOneBy(['username' => $username]);
              
	$login    = $credentials[self::USERNAME];
	$password = $credentials[self::PASSWORD];
        
	if (!$user) {
            throw new \Nette\Security\AuthenticationException("User $username not found.", self::IDENTITY_NOT_FOUND);
	}
        
	if (!$bcrypt->verify($password, $user->password)) {
            throw new \Nette\Security\AuthenticationException("Invalid password.", self::INVALID_CREDENTIAL);
	}
		
	$identity = new \Nette\Security\Identity($user->id);
        
	$identity->first_name = $user->getFirstName();
        $identity->last_name = $user->getLastName();
        $identity->username = $user->getUsername();

	return $identity;
    }
    
}
