<?php

namespace App\Model;

class UsersManager {

    public $em;
    
    /** @var Nette\Security\User @inject*/
    public $user;

    public function __construct(\Kdyby\Doctrine\Registry $registry) {
        $this->em = $registry->getManager('noetic');
    }

    public function getUsersList() {

        $qb = $this->em->createQueryBuilder();

        $qb->select('u')
                ->from(\App\Users::class, 'u')
                ->orderBy('u.last_name', 'ASC')
        ;

        $noetics = $qb->getQuery()->getResult();

        return $noetics;
    }
    
    public function getUser($id){
        return $this->em->getRepository(\App\Users::class)->find($id);
    }
}
