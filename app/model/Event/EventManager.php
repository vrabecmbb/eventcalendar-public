<?php

namespace App\Model;

class EventManager {

    /** @var \Kdyby\Doctrine\EntityManager */
    public $em;

    /** @var EventTypes @inject */
    private $eventTypes;
   
    function __construct(\Kdyby\Doctrine\EntityManager $em) {
        $this->em = $em;
        //$this->eventTypes = $eventTypes;
    }

        public function addEvent(\App\Event $event) {

        $this->em->persist($event);
        
        $this->em->flush();

        $event->id = $event->getId();

        return $event;
    }

    public function addReminder(\App\Reminder $reminder) {

        $this->em->persist($reminder);

        $this->em->flush();

        return $this;
    }

    public function updateReminder(\App\Reminder $reminder) {
        $this->em->persist($reminder);

        $this->em->flush();

        return $this;
    }

    public function findById($id) {
        $event = $this->em->getRepository(\App\Event::getClassName())->find($id);

        return $event;
    }

    public function getEvent($id) {
        return $this->em->find(\App\Event::getClassName(), $id);
    }

    /**
     * Returns first child event of copied event
     * @param type $parent_id
     */
    public function getFirstChildEvent($parent_id) {
        $qb = $this->em->createQueryBuilder();
        $qb->addSelect('e')
                ->from(\App\Event::getClassName(), 'e')
                ->where('e.owner_event=:parent')
                ->setParameter('parent', $parent_id)
                ->orderBy('e.id', 'ASC')
                ->setMaxResults(1)
        ;

        return $qb->getQuery()->getSingleResult();
    }

    /**
     * Returns all event that are child
     * @param type $parent_id
     */
    public function getAllChildren($parent_id) {
        $qb = $this->em->createQueryBuilder();
        $qb->addSelect('e')
                ->from(\App\Event::getClassName(), 'e')
                ->where('e.owner_event=:parent')
                ->setParameter('parent', $parent_id)
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * remove event
     */
    public function removeEvent(\App\Event $event) {
        
        $children = $this->getAllChildren($event->id);
            if (count($children) > 1){
                $oldestSon = $children[1];
                foreach($children as $child){
                    $child->owner_event = $oldestSon;
                }
            } else {
                $event->owner_event = NULL;
                $this->em->persist($event);
                $this->em->flush();
            }
        
        $this->em->beginTransaction();
            
        try{
            $this->em->remove($event);
            $reminder = $event->reminder;
                 
            if ($reminder){     
                $this->em->remove($reminder);
            }
            $this->em->flush();
            $this->em->commit();
        } catch(Exception $e){
            $this->em->rollback();
            throw new Exception($e);
        }
        
        return $this;
    }   

    /**
     * Flag event as it is done
     * @param \App\DoneEvents $done_event
     * @return $this
     */
    public function commitEvent(\App\DoneEvents $done_event) {
        $this->em->persist($done_event);

        $this->em->flush();

        return $this;
    }

    /** Parse name of users that will be notified about event * */
    public function getReminderUsers(\App\Reminder $reminder) {
        $toRet = [];

        foreach ($reminder->getUsers() as $user) {
            $toRet[] = $user->getName();
        }

        return $toRet;
    }

    /**
     * Return events that are not committed and expired until actual date
     */
    public function getExpiredEvents() {

        $now = new \DateTime;

        $qb = $this->em->createQueryBuilder()
                ->addSelect('e')
                ->from(\App\Event::getClassName(), 'e')
                ->where('e.start_time < :now')
                ->setParameter('now', $now)
                ->orderBy('e.start_time')
        ;

        $result = $qb->getQuery()->getResult();
        $toRet = [];

        foreach ($result as $item) {
            if (!$item->done_event) {
                $toRet[] = $item;
            }
        }

        return $toRet;
    }

    /**
     * Return events by day
     * @param \DateTime $day
     * @return array
     */
    public function getByDay(\DateTime $day) {
        $start = $day->setTime(0, 0, 0);
        $end = clone($start);
        $end->setTime(23, 59, 59);

        $qb = $this->em->createQueryBuilder();
        $qb->addSelect('e')
                ->from(\App\Event::class, 'e')
                ->where('e.start_time >= :start')
                ->andWhere('e.start_time <= :end')
                ->orWhere($qb->expr()->andX(
                                $qb->expr()->gte('e.end_time', ':end'), $qb->expr()->lte('e.end_time', ':end')
                        )
                )
                ->orderBy('e.start_time')
                ->setParameters(['start' => $start, 'end' => $end])
        ;

        $result = $qb->getQuery()->getResult();

        $toRet = [];
        foreach ($result as $item) {
            $toRet[] = $this->humanizeEvent($item);
        }

        return $toRet;
    }

    public function humanizeEvent(\App\Event $event) {

        $ret = [
            'title' => $event->title,
            'description' => $event->description,
            'start_time' => $event->start_time->format(\Dates\Dates::SK),
            'start_time_hm' => $event->start_time->format('H:i'),
            'end_time' => $event->end_time ? $event->end_time->format(\Dates\Dates::SK) : '',
            'object' => $this->recognizeObject($event), //$event->scada_id ? $event->scada_id->name : $event->fve_id->name,
            'icon' => $this->getIcon($event)
        ];

        return $ret;
    }

    private function recognizeObject(\App\Event $event) {
        $eventType = EventTypeRecognizer::recognizeEvent($event);
        return $eventType->getName();
    }

    private function getIcon(\App\Event $event) {
        $eventType = EventTypeRecognizer::recognizeEvent($event);
        return $eventType->getIcon();
    }

    /**
     * Get events by its from and to range
     * @param \DateTime $from
     * @param \DateTime $to
     * @return type
     */
    public function getLimitedEvents(\DateTime $from, \DateTime $to) {

        $qb = $this->em->createQueryBuilder()
                ->addSelect('e')
                ->from(\App\Event::getClassName(), 'e')
                ->where('e.start_time > :from')
                ->andWhere('e.start_time < :to')
                ->setParameters(['from' => $from, 'to' => $to])
                ->orderBy('e.start_time', 'ASC')
        ;

        return $qb->getQuery()->getResult();
    }

    public function getReminders(\DateTime $start, \DateTime $end) {

        $qb = $this->em->createQueryBuilder()
                ->addSelect('e')
                ->from(\App\Reminder::class, 'e')
                ->where('e.time >= :start')
                ->andWhere('e.time < :end')
                ->setParameters(['start' => $start, 'end' => $end])
        ;

        $ret = $qb->getQuery()->getResult();

        return $ret;
    }

    /**
     * Return events that becomes to parent event
     * @param type $owner_event
     * @return type
     */
    public function getChildren($owner_event) {
        
        $qb = $this->em->createQueryBuilder()
                ->addSelect('e')
                ->from(\App\Reminder::class, 'e')
                ->where('e.owner_event >= :owner_event_id')
                ->setParameters(['owner_event_id' => $owner_event->id])
                ->orderBy('e.start_time', 'ASC')
        ;

        $ret = $qb->getQuery()->getResult();

        return $ret;
    }
    
    public function getMinMaxBudget(){
        $qb = $this->em->createQueryBuilder()
                ->addSelect('MAX(e.budget) AS max_budget, MIN(e.budget) AS min_budget')
                ->from(\App\Event::class,'e')
        ;
        
        $ret = $qb->getQuery()->getSingleResult();
        
        return $ret;
    }
    
    public function getCategoryDropdowns($object_type){
        
        $qb = $this->em->createQueryBuilder()
                ->select('c')
                ->from(\App\Categories::class, 'c')
                ->where('c.type = :type')
                ->setParameter('type', $object_type)
        ;
        
        $ret = $qb->getQuery()->getResult();
        
        return $ret;
    }
    
    public function getCategoryById($id){
        $qb = $this->em->createQueryBuilder()
                ->select('c')
                ->from(\App\Categories::class, 'c')
                ->where('c.id = :id')
                ->setParameter('id', $id)
        ;
        
        return $qb->getQuery()->getSingleResult();
    }
    
    public function getCategory($name, $type){
        
        $qb = $this->em->createQueryBuilder()
                ->addSelect('c')
                ->from(\App\Categories::class, 'c')
                ->where('c.name=:name')
                ->andWhere('c.type=:type')
                ->setParameters(['name'=>$name, 'type'=>$type])
        ;

        return $qb->getQuery()->getResult();
        
        
    }
    
    public function getEventsWithCategory($category){
        
        $qb = $this->em->createQueryBuilder()
                ->addSelect('e')
                ->from(\App\Event::class, 'e')
                ->where('e.category=:category')
                ->setParameters(['category' => $category])
        ;

        return $qb->getQuery()->getResult();
        
    }
    
    public function getCategoriesByType($type){
        $qb = $this->em->createQueryBuilder()
                ->select('c')
                ->from(\App\Categories::class, 'c')
                ->where('c.type = :type')
                ->setParameter('type', $type)
        ;
        
        return $qb->getQuery()->getResult();
    }

    public function updateCategory($id, $name){
        
        $category = $this->em->find(\App\Categories::class, $id);
        
        $category->name = $name;
        
        $this->em->persist($category);
        $this->em->flush();
    }
    
    public function addCategory($name, $type){
        
        $exists = $this->getCategory($name, $type);
        
        if(!empty($exists)){
            throw new Exception('Kategória už existuje');
        }
        
        $category = new \App\Categories();
        $category->name = $name;
        $category->type = $type;
        $this->em->persist($category);
        $this->em->flush();
    }
    
    //search ids in search form
    public function search($search){
        
        $qb = $this->em->createQueryBuilder();
        $qb->addSelect('e.id')
                ->from(\App\Event::class,'e')
                ->where('e.title LIKE :search')
                ->setParameter('search', '%'.$search.'%')
        ;        
        return $qb->getQuery()->getResult();        
    }
}
