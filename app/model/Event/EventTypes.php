<?php

namespace App\Model;

class EventTypes {

    public $event;

    public function __construct(\App\Event $event) {
        $this->event = $event;
    }

    public function renderName() {
        return sprintf("<i class='%s'></i> %s", $this->getIcon(), $this->getName());
    }

    public function getIcon() {
        
    }

    public function getName() {
        
    }

    public function getType() {
        
    }

    public function getId() {
        
    }

}

class NoeticEvent extends EventTypes {

    public function getIcon() {
        return 'fas fa-sitemap';
    }

    public function getName() {
        return $this->event->scada_id->name;
    }

    public function getType() {
        return \App\Presenters\BasePresenter::NOETIC_SELECTED;
    }

    public function getId() {
        return $this->event->scada_id->id;
    }

}

class FVEEvent extends EventTypes {

    public function getIcon() {
        return 'fas fa-certificate';
    }

    public function getName() {
        return $this->event->fve_id->name;
    }

    public function getType() {
        return \App\Presenters\BasePresenter::FVE_SELECTED;
    }

    public function getId() {
        return $this->event->fve_id->id;
    }

}

class PersonEvent extends EventTypes {

    public function getIcon() {
        return 'fas fa-user';
    }

    public function getName() {
        return $this->event->person->name;
    }

    public function getType() {
        return \App\Presenters\BasePresenter::PEOPLE_SELECTED;
    }

    public function getId() {
        return $this->event->person->id;
    }

}

class VehicleEvent extends EventTypes {

    public function getIcon() {
        return 'fas fa-car';
    }

    public function getName() {
        return $this->event->vehicle->id;
    }

    public function getType() {
        return \App\Presenters\BasePresenter::VEHICLE_SELECTED;
    }

    public function getId() {
        return $this->event->vehicle->id;
    }

}

class BuildingEvent extends EventTypes {

    public function getIcon() {
        return 'fas fa-building';
    }

    public function getName() {
        return $this->event->building->name;
    }

    public function getType() {
        return \App\Presenters\BasePresenter::BUILDING_SELECTED;
    }

    public function getId() {
        return $this->event->building->id;
    }

}

class EventTypeRecognizer {

    public static function recognizeEvent(\App\Event $event) {

        if ($event->scada_id) {
            return new NoeticEvent($event);
        }
        if ($event->fve_id) {
            return new FVEEvent($event);
        }
        if ($event->person) {
            return new PersonEvent($event);
        }
        if ($event->vehicle) {
            return new VehicleEvent($event);
        }
        if ($event->building) {
            return new BuildingEvent($event);
        }
    }

}
