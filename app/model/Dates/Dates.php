<?php

namespace Dates;

/**
 * Description of Dates
 *
 * @author Administrator
 */
class Dates {

    const SK = "d.m.Y H:i";
    const SK_DATE = "d.m.Y";

    public function validateDate($format, $date) {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    public function validateRange($date1, $date2) {
        $datetime1 = \DateTime::createFromFormat(self::SK, $date1);
        $datetime2 = \DateTime::createFromFormat(self::SK, $date2);

        if ($datetime1 < $datetime2) {
            return true;
        }

        return false;
    }

    /**
     * Return datetime as string in chosen format
     * @param \DateTime $date
     * @param type $format
     * @return type
     */
    public function toFormatedString($date, $format) {
        if (!is_null($date)) {
            if ($date instanceof \DateTime) {
                return date($format, $date->getTimestamp());
            }
            return date($format, $date);
        }
        return NULL;
    }
    
    /**
     * Get differnce between two Datetimes
     * @param Datetime $date1
     * @param Datetime $date2
     */
    public function getDifference(\DateTime $date1, \DateTime $date2){
        return $date1->diff($date2);
    }

}
