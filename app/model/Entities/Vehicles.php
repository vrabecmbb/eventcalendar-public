<?php

namespace App;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="calendar_vehicles")
 */
class Vehicles extends \Kdyby\Doctrine\Entities\BaseEntity {

    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    public $id;

    /**
     * @ORM\Column(type="string")
     */
    public $make;

    /**
     * @ORM\Column(type="string")
     */
    public $model;

    public function getName() {
        return sprintf("%s %s", $this->make, $this->model);
    }
}
