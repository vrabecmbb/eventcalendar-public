<?php

namespace App;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="scada_systems_mgmt")
 */
class Noetics extends \Kdyby\Doctrine\Entities\BaseEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public $scada_system_id;
    
    /**
     * @ORM\Column(type="string")
     */
    public $scada_name;
    
    /**
     * @ORM\Column(type="string")
     */
    public $scada_url;
    
    function getId() {
        return $this->scada_system_id;
    }

    function getName() {
        return $this->scada_name;
    }

    function getUrl() {
        return $this->scada_url;
    }


}