<?php

namespace App;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="calendar_reminder")
 */
class Reminder {
    
    public function __construct(){
        $this->user_id = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public $id;

    /**
     * @ORM\ManyToOne(targetEntity="Event", inversedBy="reminder",cascade={"persist"})
     * @ORM\JoinColumn(name="event_id")
     */
    public $event_id;

    /**
     * @ORM\Column(type="datetime")
     */
    public $time;

    /**
     * @ORM\ManyToMany(targetEntity="Users", inversedBy="reminder",cascade={"persist"})
     * @ORM\JoinTable(
     *     name="reminder_mail",
     *     joinColumns={
     *         @ORM\JoinColumn(name="reminder_id", referencedColumnName="id")
     *     },
     *     inverseJoinColumns={
     *         @ORM\JoinColumn(name="mail_id", referencedColumnName="id")
     *     }
     * )
     */
    public $user_id;
    
    function setEvent($event) {
        $this->event_id = $event;
    }
    
    public function removeAllUsers(){
        $this->user_id = [];
    }

        
    public function addUser(Users $user){
        $this->user_id[] = $user;
    }
    
    public function getUsers(){
        return $this->user_id;
    }
}
