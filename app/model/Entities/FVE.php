<?php

namespace App;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fveconfig")
 */
class FVE extends \Kdyby\Doctrine\Entities\BaseEntity {

    /**
     * @ORM\Id
     * @ORM\Column(name="id",type="integer")
     */
    public $id;

    /**
     * @ORM\Column(name="url", type="string")
     */
    public $url;

    /**
     * @ORM\Column(name="name", type="string")
     */
    public $name;

    function getId() {
        return $this->id;
    }

    function getUrl() {
        return $this->url;
    }

    function getName() {
        return $this->name;
    }

}
