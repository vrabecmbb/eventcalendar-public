<?php

namespace App;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="users_mgmt")
 */
class Users extends \Kdyby\Doctrine\Entities\BaseEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     * @ORM\OneToMany(targetEntity="Event", mappedBy="owner")
     * @ORM\ManyToMany(targetEntity="Reminder", mappedBy="id", fetch="EXTRA_LAZY",cascade={"persist"})
     */
    private $id;
    
    /**
     * @ORM\Column(type="string")
     */
    private $username;
    
    /**
     * @ORM\Column(type="string")
     */
    private $password;
    
    /**
     * @ORM\Column(type="string")
     */
    private $salt;
    
    /**
     * @ORM\Column(type="string")
     */
    private $first_name;
    
    /**
     * @ORM\Column(type="string")
     */
    private $last_name;
    
    /**
     * @ORM\Column(type="string")
     */
    private $email;
    
    function getId() {
        return $this->id;
    }

    function getUsername() {
        return $this->username;
    }

    function getPassword() {
        return $this->password;
    }

    function getSalt() {
        return $this->salt;
    }

    function getFirstName() {
        return $this->first_name;
    }

    function getLastName() {
        return $this->last_name;
    }
    
    function getEmail() {
        return $this->email;
    }
    
    function getName(){
        return sprintf("%s %s", $this->first_name, $this->last_name);
    }
}