<?php

namespace App;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="calendar_events")
 */
class Event extends \Kdyby\Doctrine\Entities\BaseEntity {

    const LOCAL_SOURCE = 'local';
    const REMOTE_SOURCE = 'remote';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     * @ORM\OneToMany(targetEntity="Reminder", mappedBy="event_id")
     * @ORM\OneToMany(targetEntity="Event", mappedBy="owner_event")
     */
    public $id;

    /**
     * @ORM\Column(type="string")
     */
    public $title;

    /**
     * @ORM\Column(type="text")
     */
    public $description;

    /**
     * @ORM\Column(type="datetime")
     */
    public $start_time;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    public $end_time;

    /**
     * Id from Scada row
     * @ORM\Column(type="integer", nullable = true)
     */
    public $row_id;

    /**
     * @ORM\ManyToOne(targetEntity="Noetics")
     * @ORM\JoinColumn(name="scada_id", referencedColumnName="scada_system_id")
     */
    public $scada_id;

    /**
     * @ORM\ManyToOne(targetEntity="FVE")
     * @ORM\JoinColumn(name="fve_id", referencedColumnName="id")
     */
    public $fve_id;

    /**
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumn(name="person_id", referencedColumnName="id")
     */
    public $person;

    /**
     * @ORM\ManyToOne(targetEntity="Vehicles")
     * @ORM\JoinColumn(name="vehicle_id", referencedColumnName="id")
     */
    public $vehicle;

    /**
     * @ORM\ManyToOne(targetEntity="Buildings")
     * @ORM\JoinColumn(name="building_id", referencedColumnName="id")
     */
    public $building;

    /**
     * local if created locally, remote if created and synchronized with scada
     * @ORM\Column(type="string")
     */
    public $source;

    /**
     * true if originally created, false if repeat
     * @ORM\Column(type="boolean")
     */
    public $original;

    /**
     * One Event has One Done List
     * @ORM\OneToOne(targetEntity="DoneEvents", mappedBy="event")
     */
    public $done_event;

    /**
     * One Event has One Reminder
     * @ORM\OneToOne(targetEntity="Reminder", mappedBy="event_id")
     */
    public $reminder;

    /**
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumn(name="owner")
     */
    public $owner;

    /**
     * If repeating event, original event is set here
     * @ORM\ManyToOne(targetEntity="Event", inversedBy="id")
     * @ORM\JoinColumn(name="owner_event", referencedColumnName="id")
     */
    public $owner_event;

    /**
     *  @ORM\Column(type="datetime")
     */
    public $created;

    /**
     *  @ORM\Column(type="integer")   
     */
    public $budget;

    /**
     * @ORM\ManyToOne(targetEntity="Categories")
     * @ORM\JoinColumn(name="category",referencedColumnName="id", nullable=true)
     */
    public $category;

    function getId() {
        return $this->id;
    }

    public function setScada(Noetics $noetic) {
        $this->scada_id = $noetic;
    }

    public function setFve(FVE $fve) {
        $this->fve_id = $fve;
    }

    public function setVehicle(Vehicles $vehicle) {
        $this->vehicle = $vehicle;
    }

    public function setBuilding(Buildings $building) {
        $this->building = $building;
    }

}
