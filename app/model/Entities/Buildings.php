<?php

namespace App;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="calendar_buildings")
 */
class Buildings extends \Kdyby\Doctrine\Entities\BaseEntity {

    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     * @ORM\GeneratedValue
     */
    public $id;

    /**
     * @ORM\Column(type="string")
     */
    public $name;
    
    /**
     * @ORM\OneToOne(targetEntity="Buildings")
     * @ORM\JoinColumn(name="parent_building",referencedColumnName="id")
     * @ORM\GeneratedValue
     */
    public $parent_id;

    /**
     * @ORM\Column(type="string")
     */
    public $address;
}
