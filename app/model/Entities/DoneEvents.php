<?php

namespace App;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="calendar_done_events")
 */
class DoneEvents extends \Kdyby\Doctrine\Entities\BaseEntity {
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public $id;

    /**
     * @ORM\OneToOne(targetEntity="Event", inversedBy="done_event")
     * @ORM\JoinColumn(name="event_id", referencedColumnName="id")
     */
    public $event;

    /**
     * @ORM\Column(type="datetime")
     */
    public $date;

     /**
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    public $user; 
    
}
