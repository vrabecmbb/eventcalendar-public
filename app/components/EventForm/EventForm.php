<?php

namespace Components;

use Nette\Application\UI\Form;
use App\Model\EventTypeRecognizer;

class EventForm extends BaseControl {

    const EDIT_MODE = 'edit';
    const INSERT_MODE = 'insert';
    const COMPLETE_MODE = 'complete';
    const REPEATER_DAILY = 'daily';
    const REPEATER_WEEKLY = 'weekly';
    const REPEATER_MONTHLY = 'monthly';
    const REPEATER_YEARLY = 'yearly';
    const REMINDER_MINUTES = 'minutes';
    const REMINDER_HOURS = 'hours';
    const REMINDER_DAYS = 'days';
    const REMINDER_WEEKS = 'weeks';

    /** @var \Kdyby\Doctrine\Registry */
    private $registry;

    /** @var \Kdyby\Doctrine\EntityManager */
    private $noetic_em;

    /** @var \App\Model\UsersManager */
    private $usersManager;

    /** @var \App\Model\EventManager */
    private $eventManager;

    /** @var \App\Model\NoeticManager */
    private $noeticManager;

    /** @var \App\Model\FVEManager */
    private $fveManager;

    /** @var \App\Model\VehiclesManager */
    private $vehiclesManager;

    /** @var \App\Model\BuildingsManager */
    private $buildingsManager;

    /** @var \Nette\Security\User */
    private $owner;

    /** */
    private $mode;

    /** @var \App\Event */
    private $event;

    /** @var \Dates\Dates @inject */
    public $dates;

    /** @var \App\Model\EventTypes @inject */
    //public $eventTypes;

    /** @var \Components\Factories\IEleObjectChangerFactory @inject */
    public $eleObjectFactory;

    /** @var \Components\Factories\IEventMailChooserFactory @inject */
    public $eventMailChooserFactory;

    /** @var array */
    public $onValidationError = [];

    /** @var array */
    public $onEventSaved;

    /** @var array */
    public $onEventCreated;

    /** @var string */
    public $repeater_stop_time;

    public function __construct($repeater_stop_time, \Kdyby\Doctrine\Registry $registry, \App\Model\UsersManager $usersManager, \App\Model\EventManager $eventManager, \App\Model\NoeticManager $noeticManager, \App\Model\FVEManager $fveManager, \App\Model\VehiclesManager $vehicleManager, \App\Model\BuildingsManager $buildingsManager) {
        $this->repeater_stop_time = $repeater_stop_time;
        $this->registry = $registry;
        $this->usersManager = $usersManager;
        $this->eventManager = $eventManager;
        $this->noeticManager = $noeticManager;
        $this->fveManager = $fveManager;
        $this->vehiclesManager = $vehicleManager;
        $this->buildingsManager = $buildingsManager;

        $this->noetic_em = $registry->getManager('noetic');
    }

    public function getMode() {
        return $this->mode;
    }

    public function createComponentEleObjectChanger() {
        $objectChanger = $this->eleObjectFactory->create();

        return $objectChanger;
    }

    public function createComponentEventMailChooser() {
        $eventMailChooser = $this->eventMailChooserFactory->create();

        return $eventMailChooser;
    }

    public function createComponentForm() {
        $form = $this->formFactory->create();

        $form->addText('title', 'Názov');

        $form->addTextArea('description', 'Popis');

        $form->addText('start_time', 'Od')
                ->setHtmlAttribute('class', 'datetimepicker')
                ->addRule(Form::FILLED, 'Dátum začiatku udalosti nebol vyplnený')
                ->addRule(function($control) {
                    $value = $control->value;
                    return $this->dates->validateDate(\Dates\Dates::SK, $value);
                }, 'Neplatný dátum začiatku udalosti')
        ;

        $form->addText('end_time', 'Do')
                ->setHtmlAttribute('class', 'datetimepicker')
                ->addCondition(Form::FILLED)
                ->addRule(function($control) {
                    $value = $control->value;
                    return $this->dates->validateDate(\Dates\Dates::SK, $value);
                }, 'Neplatný dátum konca udalosti')
        ;

        //repeater
        $form->addCheckbox('repeater_is_set')
                ->setHtmlAttribute('id', 'form_event_repeater')
        ;

        $form->addSelect('repeater_period_type', 'Typ periódy', [self::REPEATER_DAILY => 'Denne', self::REPEATER_WEEKLY => 'Týždenne', self::REPEATER_MONTHLY => 'Mesačne', self::REPEATER_YEARLY => 'Ročne']);

        $form->addText('repeater_periodicity', 'Perióda opakovania');

        $form->addText('repeater_stop_time', 'Opakovať dokedy')
                ->setAttribute('class', 'datepicker')
        ;

        //reminder
        $form->addCheckbox('reminder_is_set')
                ->setHtmlAttribute('id', 'form_event_reminder')
        ;

        $form->addText('budget', 'Financie');

        $form->addText('reminder_period_time', 'Hodnota pred udalosťou');

        $form->addSelect('reminder_period_type', 'Typ', [self::REMINDER_MINUTES => 'min', self::REMINDER_HOURS => 'hodín', self::REMINDER_DAYS => 'dní', self::REMINDER_WEEKS => 'týždňov']);

        $form->addHidden('reminder_mail')
                ->setHtmlAttribute('id', 'reminder_mails')
        ;

        $form->addHidden('object_type')
                ->setHtmlAttribute('id', 'object_type')
                ->setRequired("Je potrebné vybrať typ objektu")
        ;

        $form->addHidden('object_id')
                ->setHtmlAttribute('id', 'object_id')
                ->setRequired("Je potrebné vybrať objekt")
        ;
        
        $form->addSelect('object_category_select', 'Kategória', [])
                ->setHtmlAttribute('id','object_category')
                ->setHtmlAttribute('name','object_category')
                ->setHtmlAttribute('class','form-control')
        ;

        $form->addHidden('object_category')
                ->setHtmlAttribute('id', 'object_category_hidden')
        ;

        $this->mode == self::EDIT_MODE ? $form->addSubmit('submit', 'Uložiť zmeny') : $form->addSubmit('submit', 'Vytvoriť');

        $form->onSuccess[] = [$this, 'processForm'];

        $form->onValidate[] = function($form) {
            if (!empty($form->values['end_time']) && ( $form['end_time']->value < $form['start_time']->value)) {

                $form->addError('Koniec udalosti musí byť neskôr ako začiatok udalosti');
            }

            if ($form->values['reminder_is_set']) {
                $reminder_when = $form->values['reminder_period_time'];
                $reminder_mails = $form->values['reminder_mail'];

                if ((int) $reminder_when <= 0) {
                    $form->addError('Čas upozornenia pred udalosťou nemá správnu hodnotu');
                } elseif (empty($reminder_mails)) {
                    $form->addError('Používatelia, ktorí budú na udalosť upozornení musia byť nastavení');
                }
            }

            if ($form->values['repeater_is_set']) {

                $today = new \DateTime;

                //repeater_stop_time
                if (!empty($form->values['repeater_stop_time']) && !$this->dates->validateDate(\Dates\Dates::SK_DATE, $form->values['repeater_stop_time'])) {
                    $form->addError('Konečný dátum opakovania má neplatný formát');
                }

                $repeater_stop_time = \DateTime::createFromFormat('d.m.Y', $form->values['repeater_stop_time']);
                if (!empty($form->values['repeater_stop_time']) && $today->format(\Dates\Dates::SK_DATE) >= $repeater_stop_time) {
                    $form->addError('Konečný dátum opakovania musí byť neskorší ako dnešný dátum');
                }
            }
        };

        $form->onError = function($form) {
            $this->fireCallbacks($this->onValidationError, $form->errors);
        };

        if (!is_null($this->event)) {
            $defaults = $this->prepareFormValues();
        }

        if ($this->mode == self::EDIT_MODE) {
            $form->setDefaults($defaults);
        }

        return $form;
    }

    /**
     * In insert mode, set selected date to input
     */
    public function setDate($day, $month, $year) {
        $this['form']->setDefaults(['start_time' => sprintf("%02d.%02d.%d 00:00", $day, $month, $year)]);
    }

    public function prepareFormValues() {

        $event = $this->event;

        $event instanceof \App\Event;

        //initialize persistance collection
        if ($event->reminder) {
            $event->reminder->getUsers()->count();
        }

        if ($this->event->reminder) {
            $remindered_users = $event->reminder->getUsers()->toArray();
        }

        if (!empty($this->event->owner_event)) {
            //first child event to calculate repeating event data
            $first_child_event = $this->eventManager->getFirstChildEvent($event->owner_event->id);

            $repeater_interval = $this->getRepeaterPeriod();
            $repeat_data = $this->getRepeaterDataFromInterval($repeater_interval);
        }
        
        $eventType = EventTypeRecognizer::recognizeEvent($event);
               
        $defaults = [
            'title' => $event->title,
            'description' => $event->description,
            'start_time' => $event->start_time->format(\Dates\Dates::SK),
            'end_time' => $event->end_time ? $event->end_time->format(\Dates\Dates::SK) : "",
            'repeater_is_set' => $event->owner_event ? TRUE : FALSE,
            'repeater_period_type' => isset($repeat_data) ? $repeat_data[1] : self::REPEATER_DAILY,
            'repeater_periodicity' => isset($repeat_data) ? $repeat_data[0] : "",
            'repeater_stop_time' => '',
            'reminder_is_set' => $event->reminder ? TRUE : FALSE,
            'reminder_period_time' => $event->reminder ? $this->calculateReminderTime()[0] : '',
            'reminder_period_type' => $event->reminder ? $this->calculateReminderTime()[1] : self::REMINDER_MINUTES,
            'reminder_mail' => $event->reminder ? $this->getReminderMailEditForm($remindered_users) : '',
            'object_type' => $eventType->getType(),
            'object_id' => $eventType->getId(),
            'budget' => $event->budget,
        ];
        
        return $defaults;
    }

    public function setMode($mode) {
        $this->mode = $mode;
    }

    public function setEvent($event) {
        $this->event = $event;
    }

    public function processForm($form) {

        $values = $form->getValues();

        $now = new \DateTime;

        $title = $values['title'];
        $description = $values['description'];
        $start_time = \DateTime::createFromFormat('d.m.Y H:i', $values['start_time']);
        $end_time = !empty($values['end_time']) ? \DateTime::createFromFormat('d.m.Y H:i', $values['end_time']) : NULL;
        $reminder_is_set = $values['reminder_is_set'];
        $reminder_period_time = $values['reminder_period_time'];
        $budget = $values['budget'];
        $repeater_stop_time_is_set = $values['repeater_is_set'];
        //$repeater_stop_time = $values['repeater_stop_time'];
        $users = explode(',', $values['reminder_mail']);
        $object_category = $values['object_category'];
        $object_type = $values['object_type'];
        $object_id = $values['object_id'];
        
        if ($this->mode == self::INSERT_MODE) {
            $event = new \App\Event;
        } else {
            $event = $this->event;
        }

        $event->title = $title;
        $event->description = $description;
        $event->start_time = $start_time;
        $event->end_time = $end_time;
        $event->source = \App\Event::LOCAL_SOURCE;
        $event->original = TRUE;
        $event->owner = $this->usersManager->getUser($this->owner->getId());
        $event->owner_event = NULL;
        $event->created = $now;
        $event->budget = $budget;
        $event->category = $object_category;
        
        if (!empty($object_category) || $object_category > 0){
            $event->category = $this->eventManager->getCategoryById($object_category);
        }else {
            $event->category = NULL;
        }
       
        if ($object_type === \App\Presenters\BasePresenter::NOETIC_SELECTED) {
            $noetic = $this->noeticManager->getById($object_id);
            $event->setScada($noetic);
        } elseif ($object_type === \App\Presenters\BasePresenter::FVE_SELECTED) {
            $fve = $this->fveManager->getById($object_id);
            $event->setFve($fve);
        } elseif ($object_type === \App\Presenters\BasePresenter::PEOPLE_SELECTED) {
            $user = $this->usersManager->getUser($object_id);
            $event->setPerson($user);
        } elseif ($object_type === \App\Presenters\BasePresenter::VEHICLE_SELECTED) {
            $vehicle = $this->vehiclesManager->getVehicle($object_id);
            $event->setVehicle($vehicle);
        } else {
            $building = $this->buildingsManager->getBuilding($object_id);
            $event->setBuilding($building);
        }
        
        //begin transaction  

        if ($this->mode == self::INSERT_MODE) {
            $saved_event = $this->eventManager->addEvent($event);
        } else {
            $saved_event = $event;
        }
        $this->noetic_em->beginTransaction();
        try {
            if ($reminder_is_set) {
                if ($this->mode == self::INSERT_MODE || $this->event->reminder == NULL) {
                    $reminder = new \App\Reminder();
                } else {
                    $reminder = $this->event->reminder;
                }

                $reminder->time = $this->setReminderTime($event, $values['reminder_period_time'], $values['reminder_period_type']);

                if ($this->mode == self::EDIT_MODE && $event->reminder != NULL) {
                    $event->reminder->removeAllUsers();
                }

                foreach ($users as $user) {
                    $user_entity = $this->usersManager->getUser($user);
                    $reminder->addUser($user_entity);
                }
                
                //work with reminder
                if ($this->mode == self::INSERT_MODE) {
                    $reminder->setEvent($saved_event);
                    $this->eventManager->addReminder($reminder);
                } else {
                    $reminder->setEvent($event);
                    $this->eventManager->updateReminder($reminder);
                }
               
                //$saved_event->reminder = $reminder;
                //$this->noetic_em->merge($saved_event);
                //$this->noetic_em->flush();
            }

            //repeating events
            if ($repeater_stop_time_is_set) {
                $repeater_period_type = $values['repeater_period_type'];
                $repeater_stop_time = !empty($values['repeater_stop_time']) ? $values['repeater_stop_time'] : $this->repeater_stop_time;
                $repeater_periodicity = $values['repeater_periodicity'];

                $interval = $this->createInterval($repeater_period_type, $repeater_periodicity, $repeater_stop_time);

                $iteration_date = clone($event->start_time);
                $iteration_stop_date = \DateTime::createFromFormat('d.m.Y', $repeater_stop_time);
                $end_time_interval = $this->calculateEndTimeInterval($event->start_time, $event->end_time);

                //if event is owner event (parent), remove all child events before adding them again
                $child_events = $this->eventManager->getAllChildren($event->id);
                
                foreach ($child_events as $child) {
                    $this->eventManager->removeEvent($child);
                }

                //clone events
                
                while ($iteration_date <= $iteration_stop_date) {
                    $iteration_date = $iteration_date->add($interval);
                    if ($iteration_date <= $iteration_stop_date) {
                        $iteration_event = clone($event);
                        $iteration_event->start_time = $iteration_date;
                        if ($end_time_interval instanceof \DateInterval) {
                            $end_time = clone($iteration_date);
                            $iteration_event->end_time = $end_time->add($end_time_interval);
                            $iteration_event->id = NULL;
                            $iteration_event->owner_event = $event;
                        }
                        
                        //set original event to copied event
                        $iteration_event->owner_event = $saved_event;
                        $this->eventManager->addEvent($iteration_event);

                        if ($reminder_is_set) {
                            $copied_reminder = new \App\Reminder();
                            $copied_reminder->time = $this->setReminderTime($iteration_event, $values['reminder_period_time'], $values['reminder_period_type']);
                            $copied_reminder->user_id = $reminder->user_id;
                            $copied_reminder->setEvent($iteration_event);

                            $this->eventManager->addReminder($copied_reminder);
                        }
                    }
                }
            }
            $this->noetic_em->commit();
            $this->noetic_em->persist($event);
            $this->noetic_em->flush();
        } catch (Exception $e) {
            $this->noetic_em->rollback();
        }

        $this->mode == self::INSERT_MODE ? $this->fireCallbacks($this->onEventCreated, $event) : $this->fireCallbacks($this->onEventSaved, $event);
    }

    /**
     * Return string for edit form contains user's id to be reminder about event start
     */
    private function getReminderMailEditForm($users) {
        $array = array_map(function($item) {
            return $item->id;
        }, $users);

        return implode(",", $array);
    }

    /**
     * Create DateInterval from form input
     * @param type $repeater_period_type
     * @param type $repeater_periodicity
     * @return \DateInterval
     */
    private function createInterval($repeater_period_type, $repeater_periodicity) {

        switch ($repeater_period_type) {
            case self::REPEATER_DAILY:
                $interval_suffix = 'D';
                break;
            case self::REPEATER_MONTHLY:
                $interval_suffix = 'M';
                break;
            case self::REPEATER_WEEKLY:
                $interval_suffix = 'W';
                break;
            case self::REPEATER_YEARLY:
                $interval_suffix = 'Y';
        }

        $interval = new \DateInterval('P' . $repeater_periodicity . $interval_suffix);

        return $interval;
    }

    /**
     * Returns reminder data for edit form
     * @return array [value before event, time_type(hour, minute, days, years,..)]
     */
    private function getReminderDataFromInterval(\DateInterval $interval) {

        //return [value, type]

        if ($interval->d > 0) {
            return [$interval->d, self::REMINDER_DAYS];
        }
        if ($interval->h > 0) {
            return [$interval->h, self::REMINDER_HOURS];
        }
        if ($interval->i > 0) {
            return [$interval->i, self::REMINDER_MINUTES];
        }
    }

    private function getRepeaterDataFromInterval(\DateInterval $interval) {

        if ($interval->y > 0) {
            return [$interval->y, self::REPEATER_YEARLY];
        }
        if ($interval->m > 0) {
            return [$interval->m, self::REPEATER_MONTHLY];
        }
        if ($interval->d > 0) {
            return [$interval->d, self::REPEATER_DAILY];
        }
    }

    private function calculateReminderTime() {
        $event_time = $this->event->start_time;
        $reminder_time = $this->event->reminder->time;

        return $this->getReminderDataFromInterval($event_time->diff($reminder_time));
    }

    private function calculateEndTimeInterval($start_time, $end_time) {
        if ($end_time instanceof \DateTime) {
            return $start_time->diff($end_time);
        }
        return NULL;
    }

    //calculate repeatInterval 
    private function getRepeaterPeriod() {
        $event = $this->event;
        $child_event = $this->eventManager->getFirstChildEvent($event->owner_event);

        return $this->dates->getDifference($event->owner_event->start_time, $child_event->start_time);
    }

    public function setOwner(\Nette\Security\User $owner) {
        $this->owner = $owner;
    }

    public function setReminderTime($event, $time, $time_type) {
        $event_time = clone($event->start_time);
        $interval = date_interval_create_from_date_string(sprintf('%d %s', $time, $time_type));

        return $event_time->sub($interval);
    }

    /** Ajax call **/
    public function handleGetCategoryOptions($object_type) {
        if ($this->presenter->isAjax()) {

            $dropdowns = $this->eventManager->getCategoryDropdowns($object_type);
            $ret[] = ['id'=>0, 'name'=> '------------------------'];

            foreach ($dropdowns as $option) {
                $ret[] = [
                    'id' => $option->id,
                    'name' => $option->name
                ];
            }

            echo \Nette\Utils\Json::encode($ret, 1);
            die();
        }
    }
    
    /** Ajax call **/
    public function handleGetSelectedType(){
        if ($this->presenter->isAjax()){
            if ($this->event){
                $type = $this->eventTypes->getEventType($this->event);
            } else {
                $type = \App\Presenters\BasePresenter::NOETIC_SELECTED;
            }
            
            echo \Nette\Utils\Json::encode(['type' => $type]);
            die();
        }
    }

    public function render() {
        $this->template->setFile(__DIR__ . '/template.latte');

        $this->template->render();
    }

}
