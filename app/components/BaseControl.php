<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Components;

class BaseControl extends \Nette\Application\UI\Control{
    
    /** @var Factories\FormFactory @inject */
    public $formFactory;
    
    public function __construct() {
        parent::__construct();
    }
    
    protected function fireCallbacks($callbacks, $args = []){
        foreach($callbacks as $callback){
            call_user_func($callback, $args);
        }
    }
}
