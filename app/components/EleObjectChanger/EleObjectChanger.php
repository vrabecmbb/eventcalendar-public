<?php

namespace Components;

class EleObjectChanger extends BaseControl {

    /** @var \App\Model\NoeticManager */
    private $noeticManager;

    /** @var \App\Model\FVEManager */
    private $fveManager;

    /** @var \App\Model\UsersManager */
    private $usersManager;
    
    /** @var \App\Model\VehiclesManager */
    private $vehicleManager;
    
    /** @var \App\Model\BuildingsManager */
    private $buildingManager;
    
    function __construct(\App\Model\NoeticManager $noeticManager, \App\Model\FVEManager $fveManager, \App\Model\UsersManager $usersManager, \App\Model\VehiclesManager $vehicleManager, \App\Model\BuildingsManager $buildingManager) {
        $this->noeticManager = $noeticManager;
        $this->fveManager = $fveManager;
        $this->usersManager = $usersManager;
        $this->vehicleManager = $vehicleManager;
        $this->buildingManager = $buildingManager;
    }

        private function prepareNoetics() {
        $noetics = $this->noeticManager->getNoeticsList();

        $toRet = [];
        foreach ($noetics as $noetic) {
            $toRet[$noetic->getId()] = $noetic->getName();
        }

        return $toRet;
    }

    private function prepareFVE() {
        $fves = $this->fveManager->getFVEList();

        $toRet = [];
        foreach ($fves as $fve) {
            $toRet[$fve->getId()] = $fve->getName();
        }

        return $toRet;
    }

    private function preparePeople() {
        $people = $this->usersManager->getUsersList();
        
        $toRet = [];
        foreach ($people as $user) {
            $toRet[$user->getId()] = $user->getName();
        }
        
        return $toRet;
    }
    
    private function prepareVehicles(){
        
        $vehicles = $this->vehicleManager->getVehiclesList();
        
        $toRet = [];
        foreach($vehicles as $vehicle){
            $toRet[$vehicle->id] = $vehicle->id;
        }
        
        return $toRet;
    }
    
    private function prepareBuildings(){
        
        $buildings = $this->buildingManager->getBuildingsList();
        
        $toRet = [];
        foreach($buildings as $building){
            $toRet[$building->id] = $building->name;
        }
        
        return $toRet;
    }

    public function render() {
        $this->template->setFile(__DIR__ . '/template.latte');
        $this->template->noetics = $this->prepareNoetics();
        $this->template->fves = $this->prepareFVE();
        $this->template->people = $this->preparePeople();
        $this->template->vehicles = $this->prepareVehicles();
        $this->template->buildings = $this->prepareBuildings();
        $this->template->render();
    }

}
