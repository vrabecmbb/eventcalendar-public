<?php

namespace Components;

class BudgetEventsFilter extends BaseControl {

    /** @var \Nette\Http\SessionSection */
    private $sessionSection;

    /** @var \Nette\Http\Session */
    private $session;
    
    /** @var \App\Model\EventManager */
    private $eventManager;

    function __construct(\Nette\Http\Session $session, \App\Model\EventManager $eventManager) {
        $this->session = $session;
        $this->eventManager = $eventManager;
    }

        public function setSessionNamespace($namespace) {
        $this->sessionSection = $this->session->getSection($namespace);
        return $this;
    }

    public function setFrom($value) {
        $this->sessionSection->offsetSet('budget_from', (int)$value);
        return $this;
    }

    public function setTo($value) {
        $this->sessionSection->offsetSet('budget_to', (int)$value);
        return $this;
    }
    
    public function getFrom(){
        if ($this->sessionSection->offsetGet('budget_from')){
            return $this->sessionSection->offsetGet('budget_from');
        }
        else return NULL;
    }
    
    public function getTo(){
        if ($this->sessionSection->offsetGet('budget_to')){
            return $this->sessionSection->offsetGet('budget_to');
        }
        else return $this->getMinMaxBudget()['max_budget'];
    }
    
    
    public function handleChangeRange($from,$to){
        
        $this->setFrom($from);
        $this->setTo($to);
        
        $this->redirect('this');
        //return $this;
    }

    private function getMinMaxBudget() {
        $minMax = $this->eventManager->getMinMaxBudget();

        return $minMax;
    }

    public function render() {
        /* if (is_null($this->period)) {

          if (is_null($this->sessionSection->offsetGet('period'))) {
          $this->period = $this->defaultPeriod;
          $this->sessionSection->offsetSet('period', $this->period);
          }

          $this->period = $this->sessionSection->offsetGet('period');
          } else {
          $this->sessionSection->offsetSet('period', $this->period);
          } */

        //$this->template->periods = $this->periods;
        //$this->template->selectedPeriod = $this->periods[$this->period];

        $this->template->setFile(__DIR__ . '/template.latte');
        $this->template->budget_from = $this->getFrom();
        $this->template->budget_to = $this->getTo();
        $this->template->minmaxbudget = $this->getMinMaxBudget();
        $this->template->render();
    }

}
