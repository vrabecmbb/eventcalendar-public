<?php

namespace Components;

/**
 * Description of EventsSearchForm
 *
 * @author vrabec
 */
class EventsSearchForm extends BaseControl{
    
    /** @var array */
    public $onResults = [];
    
    protected function createComponentForm(){
        
        $form = $this->formFactory->create();
        
        $form->addText('search', 'Search');
        $form->addSubmit('btnSubmit');
        
        $form->onSuccess[] = [$this, 'onSuccess'];
        
        return $form;
        
    }
    
    public function setSearch($search){
        $this['form']['search']->setValue($search);
        
        return $this;
    }
    
    public function handleSearch($search){
        $this->fireCallbacks($this->onResults, $search);
    }
    
    public function render(){
        $this->template->setFile(__DIR__.'/template.latte');
        
        $this->template->render();
    }
}
