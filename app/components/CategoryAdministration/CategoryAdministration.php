<?php

namespace Components;

/**
 * Description of CategoryAdministration
 *
 * @author vrabec
 */
class CategoryAdministration extends BaseControl{
    
     /** @var string */
    public $eventTypes;
    
    /** @var \App\Model\EventManager */
    private $eventsManager;
    
    /** @var \Kdyby\Doctrine\EntityManager */
    private $em;
    
    function __construct($eventTypes, \App\Model\EventManager $eventsManager, \Kdyby\Doctrine\EntityManager $em) {
        $this->eventTypes = $eventTypes;
        $this->eventsManager = $eventsManager;
        $this->em = $em;
    }

    
    private function prepareList(){
        
        $types = $this->eventTypes;
        
        $toRet = [];
        
        foreach ($types as $item) {
            $item['categories'] = $this->eventsManager->getCategoriesByType($item['type']);
            $toRet[] = $item;
        }   
        
        return $toRet;
        
    }
    
    public function handleRemoveCategory($id){
        
        //get events with id
        $events = $this->eventsManager->getEventsWithCategory($id);
        
        try{
            foreach($events as $event){
                $event->category = NULL;
                $this->em->persist($event);
            }
            $this->em->flush();

            $category = $this->em->find(\App\Categories::class, $id);
            $this->em->remove($category);
            $this->em->flush();
        } catch (Exception $e){
            $this->flashMessage($e->getMessage());
        } finally {
            $this->flashMessage("Kategória bola odstránená");
        }
        
        $this->presenter->redirect('this');
        
    }
    
    public function render(){
        $this->template->setFile(__DIR__ . '/template.latte');
        
        $this->template->list = $this->prepareList();

        $this->template->render();
    }
    
}
