<?php

namespace Components;

use Nette\Application\UI\Form;

class AddCategoryForm extends BaseControl {
    
    private $eventTypes;
    
    /** @var \App\Model\EventManager */
    private $eventManager;
    
    /** @var array */
    public $onSuccess = [];
    
    /** @var array */
    public $onError = [];
    
    function __construct($eventTypes, \App\Model\EventManager $eventManager) {
        $this->eventTypes = $eventTypes;
        $this->eventManager = $eventManager;
    }

    public function createComponentForm(){
                   
       $form = $this->formFactory->create();
        
       $form->addText("name","Názov kategórie");
        
       $form->addHidden("type")
            ->setAttribute('id','event_type_hidden')   
       ;
        
       $form->addSubmit('send');
            
       $form->onSuccess[] = [$this, 'processForm'];
        
       return $form;
        
    }
    
    
    public function processForm(Form $form){
        
        $values = $form->getValues();
        
        $type = $values['type'];
        $name = $values['name'];
        
        try{
            $this->eventManager->addCategory($name, $type);
        }catch (Exception $e){
            $this->fireCallbacks($this->onError,$e->getMessage());
        }
        
        $this->fireCallbacks($this->onSuccess,"Kategória bola pridaná");
    }
    
    
    public function render(){
        
        $this->template->setFile(__DIR__.'/template.latte');
        
        $this->template->types = $this->eventTypes;
        
        $this->template->render();
    }
}
