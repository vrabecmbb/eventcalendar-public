<?php

namespace Components;

class EventMailChooser extends BaseControl {

    /** @var \App\Model\UsersManager */
    private $usersManager;

    function __construct(\App\Model\UsersManager $usersManager) {
        $this->usersManager = $usersManager;
    }

    private function prepareUsers() {
        $users = $this->usersManager->getUsersList();

        $toRet = [];
        foreach ($users as $user) {
            $toRet[$user->getId()] = sprintf("%s %s", $user->getLastName(), $user->getFirstName());
        }

        return $toRet;
    }

    public function render() {
        $this->template->setFile(__DIR__ . '/template.latte');
        $this->template->users = $this->prepareUsers();
        $this->template->render();
    }

}
