<?php

namespace Components;

class EventsObjectFilter extends BaseControl {

    /** @var \App\Model\NoeticManager */
    private $noeticsManager;
    private $type;
    private $object;

    /** @var \App\Model\FVEManager */
    private $fveManager;
    private $fves;

    /** @var \App\Model\UsersManager */
    private $usersManager;
    private $users;

    /** @var \App\Model\VehiclesManager */
    private $vehiclesManager;
    private $vehicles;

    /** @var \App\Model\BuildingsManager */
    private $buildingsManager;
    private $buildings;

    /** @var \App\Model\EventManager */
    private $eventManager;
    private $noetics;

    /** @var \Nette\Http\SessionSection */
    private $sessionSection;

    /** @var \Nette\Http\Session */
    private $session;
            
    /** @var array */        
    private $eventTypes;        

    function __construct($eventTypes, \App\Model\NoeticManager $noeticsManager, \App\Model\FVEManager $fveManager, \App\Model\UsersManager $usersManager, \App\Model\VehiclesManager $vehiclesManager, \App\Model\BuildingsManager $buildingsManager, \App\Model\EventManager $eventManager, \Nette\Http\Session $session) {
        $this->eventTypes = $eventTypes;
        $this->noeticsManager = $noeticsManager;
        $this->fveManager = $fveManager;
        $this->usersManager = $usersManager;
        $this->vehiclesManager = $vehiclesManager;
        $this->buildingsManager = $buildingsManager;
        $this->eventManager = $eventManager;
        $this->session = $session;
        $this->fves = $this->prepareFves();
        $this->noetics = $this->prepareNoetics();
        $this->vehicles = $this->prepareVehicles();
        $this->users = $this->prepareUsers();
        $this->buildings = $this->prepareBuildings();
        $this->eventManager = $eventManager;
    }

    /* public function __construct(\App\Model\NoeticManager $noeticsManager, \App\Model\FVEManager $fveManager, \App\Model\UsersManager $usersManager, \App\Model\VehiclesManager $vehiclesManager, \App\Model\BuildingsManager $buildingsManager, App\Model\EventManager $eventManager, \Nette\Http\Session $session) {
      $this->noeticsManager = $noeticsManager;
      $this->fveManager = $fveManager;
      $this->usersManager = $usersManager;
      $this->vehiclesManager = $vehiclesManager;
      $this->buildingsManager = $buildingsManager;
      $this->session = $session;
      $this->fves = $this->prepareFves();
      $this->noetics = $this->prepareNoetics();
      $this->vehicles = $this->prepareVehicles();
      $this->users = $this->prepareUsers();
      $this->buildings = $this->prepareBuildings();
      $this->eventManager = $eventManager;
      dump($this->eventManager);
      } */

    public function setSessionNamespace($namespace) {
        $this->sessionSection = $this->session->getSection($namespace);
        return $this;
    }

    public function render() {
        $this->template->setFile(__DIR__ . '/template.latte');

        //$this->sessionSection->offsetSet('object', $this->object);

        $this->template->fves = $this->fves;
        $this->template->noetics = $this->noetics;
        $this->template->users = $this->users;
        $this->template->vehicles = $this->vehicles;
        $this->template->buildings = $this->buildings;

        $this->template->objectSelected = $this->getSelected() ? $this->getSelectedName($this->getSelected()) : "Všetko";
        $this->template->categorySelected = $this->getSelected()['category'] ? $this->getSelectedCategory($this->getSelected()['category']) : "Všetky kat.";

        $type = $this->sessionSection->offsetGet('type');

        $this->template->categories = [];
        /*if ($type) {
            $categories = $this->eventManager->getCategoryDropdowns($type);

            $this->template->categories = $categories;
        }*/

        $this->template->render();
    }

    private function prepareFves() {
        $fves = $this->fveManager->getFVEList();

        $ret = [];

        foreach ($fves as $fve) {
            $ret[$fve->id] = $fve;
        }

        return $ret;
    }

    private function prepareNoetics() {
        $noetics = $this->noeticsManager->getNoeticsList();

        $ret = [];
        foreach ($noetics as $noetic) {
            $ret[$noetic->scada_system_id] = $noetic;
        }

        return $ret;
    }

    private function prepareUsers() {
        $users = $this->usersManager->getUsersList();

        $ret = [];

        foreach ($users as $user) {
            $ret[$user->id] = $user;
        }

        return $ret;
    }

    private function prepareVehicles() {
        $vehicles = $this->vehiclesManager->getVehiclesList();

        return $vehicles;
    }

    private function prepareBuildings() {
        $buildings = $this->buildingsManager->getBuildingsList();
        $ret = [];

        foreach ($buildings as $building) {
            $ret[$building->id] = $building;
        }

        return $ret;
    }

    public function setFilter($params) {

        if (!empty($params['type'])) {
            $this->type = $params['type'];
            $this->object = $params['id'];

            $this->sessionSection->offsetSet('type', $params['type']);
            $this->sessionSection->offsetSet('object', $params['id']);
        }
        if (!empty($params['category'])) {
            $category = $params['category'];

            $this->sessionSection->offsetSet('category', $category);
        }

        return $this;
    }

    public function getSelected() {

        $ret = NULL;

        $type = $this->sessionSection->offsetGet('type');
        $object = $this->sessionSection->offsetGet('object');
        $category = $this->sessionSection->offsetGet('category');

        if ($type) {
            $ret = [
                'type' => $type,
                'object' => $object,
                'category' => $category
            ];
        }

        return $ret;
    }

    public function getSelectedName($object) {
        $index = $object['object'];
        $type = $object['type'];
               
        $name = "";

        switch ($type) {
            case 'noetic':
                $name = $this->noetics[$index]->name;
                break;
            case 'fve':
                $name = $this->fves[$index]->name;
                break;
            case 'people':
                $name = $this->users[$index]->name;
                break;
            case 'vehicle':
                $name = $object['object'];
                break;
            case 'building':
                $name = $this->buildings[$index]->name;
                break;
        }

        return $name;
    }

    public function getSelectedCategory($category) {
        return $this->eventManager->getCategoryById($category)->name;
    }

    /* private function prepareObjectTypeCategory($object_type){
      $categories = $this->eventManager->getCategoryDropdowns($object_type);

      $ret = [];
      foreach ($categories as $category){
      $ret[$category->id] = $category->name;
      }

      return $ret;

      } */

    public function handleClearFilter() {
        $this->sessionSection->remove();

        $this->redirect('this');
    }

}
