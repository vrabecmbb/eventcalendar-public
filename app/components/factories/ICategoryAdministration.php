<?php

namespace Components\Factories;

interface ICategoryAdministrationFactory {

    /** @return \Components\CategoryAdministration */
    public function create();
}
