<?php

namespace Components\Factories;

/**
 * Description of IEventFormFactory
 *
 * @author Administrator
 */
interface IAddCategoryFormFactory {

    /** @return \Components\AddCategoryForm */
    public function create();
    
}
