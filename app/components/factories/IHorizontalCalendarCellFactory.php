<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IHorizontalCalendarCellFactory
 *
 * @author Administrator
 */
class IHorizontalCalendarCellFactory extends CalendarCellFactory{
	 /**
     * @param int $row
     * @param int $col
     * @return bool
     */
    public function isForDayLabel($row, $col)
    {
        return $row === 0;
    }
    /**
     * @param int $row
     * @param int $col
     * @return int
     */
    public function calcNumber($row, $col)
    {
        return ($row - 1) * 7 - $this->getCalendarStartDay() + $col;
    }
}
