<?php

namespace Components\Factories;

interface IEventsFilterFactory {

    /** @return \Components\EventsFilter */
    public function create();
}
