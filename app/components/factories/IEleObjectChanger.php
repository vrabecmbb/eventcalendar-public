<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Components\Factories;

interface IEleObjectChangerFactory{
    
    /** @return \Components\EleObjectChanger */
    public function create();
}