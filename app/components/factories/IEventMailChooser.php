<?php

namespace Components\Factories;

interface IEventMailChooserFactory {

    /** @return \Components\EventMailChooser */
    public function create();
}
