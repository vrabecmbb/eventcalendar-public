<?php

namespace Components\Factories;

interface IEventObjecFilterFactory {

    /** @return \Components\EventsObjectFilter */
    public function create();
}
