<?php

namespace Components\Factories;

/**
 * Description of IEventFormFactory
 *
 * @author Administrator
 */
interface IEventFormFactory {

    /** @return \Components\EventForm */
    public function create();
    
}
