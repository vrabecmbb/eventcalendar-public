<?php

namespace Components\Factories;

/*
 * @author Administrator
 */
interface ICalendarFactory {
    
    /** @return \Components\Calendar */
    public function create();
}
