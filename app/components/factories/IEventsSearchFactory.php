<?php

namespace Components\Factories;

interface IEventsSearchFormFactory {

    /** @return \Components\EventsSearchForm */
    public function create();
}
