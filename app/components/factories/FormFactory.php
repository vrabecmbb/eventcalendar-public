<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of formFactory
 *
 * @author marti
 */

namespace Components\Factories;

class FormFactory {

    /** return \Nette\Application\UI\Form */
    public function create() {
        $form = new \Nette\Application\UI\Form();

        return $form;
    }
}
