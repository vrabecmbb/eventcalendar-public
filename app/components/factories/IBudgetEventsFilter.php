<?php

namespace Components\Factories;

interface IBudgetEventFilterFactory {

    /** @return \Components\BudgetEventsFilter */
    public function create();
}
