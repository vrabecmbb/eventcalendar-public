<?php
 namespace Components;

class Calendar extends \blitzik\Calendar\Calendar {

    protected $numberOfDaysLabelsCharactersToTruncate;
    protected $areSelectionsActive = FALSE;

    /** @var \App\Model\EventManager $eventManager */
    private $eventManager;

    public function __construct(\App\Model\EventManager $eventManager) {
        parent::__construct('sk');
        $this->numberOfDaysLabelsCharactersToTruncate = 2;
        $this->eventManager = $eventManager;
    }

    public function getMonthName($monthNumber, $type = NULL) {
        \Nette\Utils\Validators::assert($monthNumber, 'numericint:1..12');

        $format = is_null($type) ? 'F' : 'M';

        return strtolower(\DateTime::createFromFormat('!m', $monthNumber)->format($format));
    }

    public function prepareCell($row, $col) {
        $cellNumber = $this->cellFactory->calcNumber($row, $col);
        $day = $this->calendarData[$cellNumber]->getDay();
        $date = $day->getDateTime();

        $day->events = $this->eventManager->getByDay($date);
        
        //$day->color = $this->prepareColor($day->geocaches, $day->diaries, $day->articles);
        return $this->calendarData[$cellNumber];
    }

    public function render() {
        $today = new \DateTime;
        $today->setTime(0, 0, 0);

        $template = $this->getTemplate();
        $template->setFile(__DIR__ . '/calendar.latte');

        if (isset($this->translator)) {
            $template->setTranslator($this->translator);
        }

        if (!isset($this->calendarGenerator)) {
            $this->cellFactory = new \blitzik\Calendar\Factories\HorizontalCalendarCellFactory();
            $this->calendarGenerator = new \blitzik\Calendar\Generator\CalendarGenerator($this->cellFactory);
        }

        $this->calendarData = $this->getCalendarData();
        $template->today = $today;
        $template->month = $this->month;
        $template->year = $this->year;
        $template->areSelectionsActive = TRUE;
        $template->charsToShortTo = $this->numberOfDaysLabelsCharactersToTruncate;

        $template->rows = $this->cellFactory->getNumberOfRows();
        $template->cols = $this->cellFactory->getNumberOfColumns();

        $template->getCell = function($row, $col) {
            return $this->prepareCell($row, $col);
        };

        $template->getMonthName = function ($monthNumber) {
            return $this->getMonthName($monthNumber);
        };

        $template->getMonthNameSelect = function ($monthNumber) {
            return $this->getMonthName($monthNumber, true);
        };

        //$template->links = $this->prepareLinks();

        $template->calendarBlocksTemplate = __DIR__ . '/calendarBlocks.latte';

        $template->render();
    }

}
