<?php

namespace Console\Command;

use Symfony\Component\Console;

class CopySolar extends Console\Command\Command {
    
    public $noetic_em;
    public $solar_em;

    public function __construct(\Kdyby\Doctrine\Registry $registry) {
        parent::__construct();
        $this->noetic_em = $registry->getManager('noetic');
        $this->solar_em = $registry->getManager('solar');      
    }
    
    protected function configure() {
        $this->setName('solar:copy');
    }
    
    protected function execute(Console\Input\InputInterface $input, Console\Output\OutputInterface $output) {
        
        $solar_fves = $this->solar_em->getRepository(\App\FVE::class)->findAll();
        
        $noetic_fves = $this->noetic_em->getRepository(\App\FVE::class)->findAll();
        
        foreach($noetic_fves as $item){
            $this->noetic_em->remove($item);
            $this->noetic_em->flush();
        }
        
        $output->write("FVE's removed from SCADA_MGMT");   
        
        foreach($solar_fves as $item){
            $this->noetic_em->persist($item);
            $this->noetic_em->flush();
        }
        
        $output->write("FVE's copied to SCADA_MGMT table");        
    }
}