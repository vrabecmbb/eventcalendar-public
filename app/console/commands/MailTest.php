<?php

namespace Console\Command;

use Symfony\Component\Console;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;

class MailTest extends Console\Command\Command {

    /** @var \Doctrine\ORM\EntityManager */
    private $em;

    /** @var \App\Model\EventManager */
    private $eventManager;
    private $from;

    function __construct($from, \Doctrine\ORM\EntityManager $em, \App\Model\EventManager $eventManager) {
        parent::__construct();
        $this->from = $from;
        $this->em = $em;
        $this->eventManager = $eventManager;
    }

    protected function configure() {
        $this->setName('mail:test');
    }

    protected function execute(Console\Input\InputInterface $input, Console\Output\OutputInterface $output) {
        
        $mailer = new SendmailMailer();

        $latte = new \Latte\Engine;


        $mail = new Message();
        $mail->setFrom($this->from);
        $mail->addTo('sebek@hako.sk');
        
        $mail->setHtmlBody('<b>Hello</b>');
        //dump($mail);
        $mailer->send($mail);
    }

}
