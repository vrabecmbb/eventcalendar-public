<?php

namespace Console\Command;

use Symfony\Component\Console;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;

class EventReminder extends Console\Command\Command {
    
    /** @var \Doctrine\ORM\EntityManager */
    private $em;

    /** @var \App\Model\EventManager */
    private $eventManager;
    private $from;

    function __construct($from, \Doctrine\ORM\EntityManager $em, \App\Model\EventManager $eventManager) {
        parent::__construct();
        $this->from = $from;
        $this->em = $em;
        $this->eventManager = $eventManager;
    }

    protected function configure() {
        $this->setName('event:reminder');
    }

    protected function execute(Console\Input\InputInterface $input, Console\Output\OutputInterface $output) {

        $latte = new \Latte\Engine;
        
        // nainstalujeme do $latte makra {link} a n:href
        \Nette\Bridges\ApplicationLatte\UIMacros::install($latte->getCompiler());

        $now = new \DateTime;
        $end = clone($now);
        $reminders = $this->eventManager->getReminders($now, $end->modify('+10 minutes'));

        $output->write(sprintf("%d reminders", count($reminders)));

        $mailer = new SendmailMailer();    

        foreach ($reminders as $reminder) {
            $params = [
                'event' => $reminder->event_id,
                'title' => $reminder->event_id->title,
                'id' => $reminder->event_id->id,
                'link' => 'http://kalendar.hako.sk/calendar/show/'.$reminder->event_id->id
            ];
            $mail = new Message();
            $mail->setFrom($this->from);
            $users = $reminder->getUsers();
            foreach ($users as $user) {
                $mail->addTo($user->getEmail());
            }
            $mail->setHtmlBody($latte->renderToString(__DIR__ . '/templates/email.latte', $params));
            //dump($mail);
            $mailer->send($mail);
        }
    }

    
}
