<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Presenters;

use Nette\Utils\Html;
use Nette\Http\Request;
use App\Model\EventTypeRecognizer;

class CalendarPresenter extends BasePresenter {

    /** @var \Components\Factories\IEventFormFactory @inject */
    public $eventFormFactory;

    /** @var \App\Model\NoeticManager @inject */
    public $noeticManager;

    /** @var \App\Model\FVEManager @inject */
    public $fveManager;

    /** @var \App\Model\EventManager @inject */
    public $eventManager;

    /** @var \App\Model\UsersManager @inject */
    public $userManager;

    /** @var \App\Model\VehiclesManager @inject */
    public $vehicleManager;

    /** @var \App\Model\BuildingsManager @inject */
    public $buildingManager;
    ///** @var \App\Model\EventTypes @inject */
    //public $eventTypes;
    public $day;
    public $month;
    public $year;

    public function actionDefault() {
        
    }

    public function renderDay($day, $month, $year) {
        $date = new \DateTime;
        $date->setDate($year, $month, $day);

        $this->template->date = $date->format('j. F Y');
        $nextDay = clone($date);
        $nextDay->modify('+ 1 day');
        $this->template->nextDay = [
            'day' => $nextDay->format('d'),
            'month' => $nextDay->format('m'),
            'year' => $nextDay->format('Y'),
        ];

        $previousDay = clone($date);
        $previousDay->modify('- 1 day');
        $this->template->previousDay = [
            'day' => $previousDay->format('d'),
            'month' => $previousDay->format('m'),
            'year' => $previousDay->format('Y'),
        ];

        $previousMonth = clone($date);
        $previousMonth->modify('- 1 month');
        $this->template->previousMonth = [
            'day' => $previousMonth->format('d'),
            'month' => $previousMonth->format('m'),
            'year' => $previousMonth->format('Y'),
        ];

        $nextMonth = clone($date);
        $nextMonth->modify('+ 1 month');
        $this->template->nextMonth = [
            'day' => $nextMonth->format('d'),
            'month' => $nextMonth->format('m'),
            'year' => $nextMonth->format('Y'),
        ];
    }

    /**
     * Add event
     * @param type $day
     * @param type $month
     * @param type $year
     */
    public function actionAdd($day, $month, $year) {

        $request['presenter'] = $this->getRequest()->getPresenterName();
        $request['action'] = $this->getRequest()->getParameters();

        $json_request = \Nette\Utils\Json::encode($request);

        if (!$this->user->isLoggedIn()) {
            $this->redirect("Homepage:Login", ['request' => $json_request]);
        }

        $this['eventForm']->setMode(\Components\EventForm::INSERT_MODE);
        $this['eventForm']->setDate($day, $month, $year);
    }

    /**
     * Edit event
     * @param type $id
     */
    public function actionEdit($id) {

        $event = $this->eventManager->getEvent($id);

        if (!$event) {
            $this->flashMessage("Event neexisuje", "error");
            $this->redirect("default");
        }

        if (!$this->user) {
            $request['presenter'] = $this->getRequest()->getPresenterName();
            $request['action'] = $this->getRequest()->getParameters();

            $json_request = \Nette\Utils\Json::encode($request);

            if (!$this->user->isLoggedIn()) {
                $this->redirect("Homepage:Login", ['request' => $json_request]);
            }
        }

        $day = $event->start_time->format('d');
        $month = $event->start_time->format('m');
        $year = $event->start_time->format('Y');

        $this['eventForm']->setMode(\Components\EventForm::EDIT_MODE);
        $this['eventForm']->setEvent($event);

        if ($event->done_event) {
            $this->flashMessage("Event nie je možné editovať", "error");
            $this->redirect("Day", ['day' => $day, 'month' => $month, 'year' => $year]);
        }

        if (!$event) {
            $this->flashMessage("Event neexisuje", "error");
            $this->redirect("Day", ['day' => $day, 'month' => $month, 'year' => $year]);
        }

        $this->template->mode = $this['eventForm']->getMode();
    }

    /**
     * Show event
     * @param type $id
     */
    public function actionShow($id) {

        $event = $this->eventManager->getEvent($id);

        if (!$event) {
            $this->flashMessage("Event neexisuje", "error");
            $this->redirect("default");
        }

        //prepareData
        $eventData = $this->prepareData($event);

        $this->template->eventData = $eventData;
        $this->template->event = $event;
    }

    /**
     * Prepare data to show event
     * @param \App\Event $event
     */
    private function prepareData(\App\Event $event) {

        //init   
        if ($event->reminder) {
            count($event->reminder->getUsers());
        }

        $children = $this->eventManager->getAllChildren($event->id);

        $start_times = [];

        foreach ($children as $child) {
            $start_times[] = $child->start_time->format(\Dates\Dates::SK);
        }
        $children_dates = $start_times;

        $eventType = EventTypeRecognizer::recognizeEvent($event);

        $toRet = [
            'title' => $event->title,
            'description' => $event->description,
            'start_time' => $event->start_time->format(\Dates\Dates::SK),
            'label' => $eventType->renderName(),
            'object' => $this->recognizeObject($event), //$event->scada_id ? $event->scada_id->name : $event->fve_id->name,
            'object_type' => $this->recognizeObjectType($event), //$event->scada_id ? BasePresenter::NOETIC_SELECTED : BasePresenter::FVE_SELECTED,
            'reminder_date' => $event->reminder ? $event->reminder->time->format(\Dates\Dates::SK) : FALSE,
            'reminder_users' => $event->reminder ? implode(", ", $this->eventManager->getReminderUsers($event->reminder)) : FALSE,
            'children_dates' => $children_dates,
            'budget' => $event->budget,
            'category' => $event->category
        ];

        return $toRet;
    }

    /**
     * Flag event as it is done
     * @param type $id
     */
    public function handleConfirmEvent($id) {

        //find event
        $event = $this->eventManager->findById($id);

        if (!$event) {
            $this->redirect('Calendar:day');
        }

        if (!$this->user) {
            $this->flashMessage('Používateľ musí byť prihlásený');
            $this->redirect('Calendar:day');
        }

        $now = new \DateTime;

        $user = $this->userManager->getUser($this->user->id);

        $done_event = new \App\DoneEvents();
        $done_event->event = $event;
        $done_event->date = $now;
        $done_event->user = $user;

        $this->eventManager->commitEvent($done_event);

        $this->flashMessage("Udalosť bola dokončená", "success");
        $this->redirect('Calendar:day');
    }

    /**
     * 
     * @return \Grido\Grid
     */
    public function createComponentGrid() {
        $grid = new \Grido\Grid();

        $grid->setTranslator(new \Grido\Translations\FileTranslator("sk"));

        $repository = $this->noetic_em->getRepository("App\Event");

        $start_date_start = new \DateTime;
        $start_date_start->setDate($this->year, $this->month, $this->day);
        $start_date_start->setTime(0, 0, 0);

        $start_date_end = clone($start_date_start);
        $start_date_end->setDate($this->year, $this->month, $this->day);
        $start_date_end->setTime(23, 59, 59);

        $query = $repository->createQueryBuilder('e')
                ->addSelect('e')
                ->where('e.start_time >=:start')
                ->andWhere('e.start_time <= :end')
                ->setParameters(['start' => $start_date_start, 'end' => $start_date_end])
        ;

        $model = new \Grido\DataSources\Doctrine($query);

        $grid->model = $model;

        $grid->addColumnText('id', 'ID');
        $grid->addColumnDate('start_time', 'Dátum');
        $grid->addColumnText('title', 'Názov');
        $grid->addColumnText('scada_id', 'Objekt')
                ->setCustomRender(function($item) {
                    $event = EventTypeRecognizer::recognizeEvent($item);
                    return $event->renderName();
                });

        $grid->addColumnText('done_event', 'Dokončená')
                ->setCustomRender(function($item) {
                    if (is_null($item->done_event)) {
                        $el = Html::el('span')->class('red done_event');
                        $el->setText('Nedokončená');
                        return $el;
                    } else {
                        $div = Html::el('div');
                        $el = Html::el('span')->class('green done_event');
                        $el->setText('Dokončená');
                        $div->add($el);
                        $div->add(Html::el('div')->class('small green')->setText($item->done_event->date->format('d.m.Y H:i:s')));
                        return $div->_toString();
                    }
                });

        $grid->addActionHref('show', 'Ukázať')->setCustomRender(function($item) {
            $html = Html::el('a', [
                        'href' => $this->link('show', $id = $item->id),
                        'class' => 'fas fa-eye'
            ]);

            return $html;
        });

        $grid->addActionHref('edit', 'Edit')->setCustomRender(function($item) {
            if (is_null($item->done_event) && $this->user->isLoggedIn()) {
                $html = Html::el('a', [
                            'href' => $this->link('edit', $id = $item->id),
                            'class' => 'fas fa-pencil-alt'
                ]);

                return $html;
            }
        });

        if ($this->user->isLoggedIn()) {
            $grid->addActionHref('success', 'Success')->setCustomRender(function($item) {
                if (is_null($item->done_event)) {
                    $html = Html::el('a', [
                                'href' => $this->link('confirmEvent!', $id = $item->id),
                                'onclick' => 'return confirm("Naozaj chcete označiť udalosť za vykonanú")',
                                'class' => 'fas fa-check'
                    ]);

                    return $html;
                }
            });


            $grid->addActionHref('remove', 'Remove')->setCustomRender(function($item) {
                $html = Html::el('a', [
                            'href' => $this->link('removeEvent!', $id = $item->id),
                            'onclick' => 'return confirm("Naozaj chcete odstrániť túto udalosť?")',
                            'class' => 'fa fa-trash'
                ]);

                return $html;
            });
        }

        return $grid;
    }

    public function handleRemoveEvent($id) {

        $event = $this->eventManager->findById($id);

        if (!$event) {
            $this->redirect('Calendar:day');
        }

        if (!$this->user) {
            $this->flashMessage('Používateľ musí byť prihlásený');
            $this->redirect('this');
        }

        try {
            $this->eventManager->removeEvent($event);
        } catch (Exception $e) {
            $this->flashMessage($e);
            $this->redirect('this');
        } finally {
            $this->flashMessage("Udalosť bola odstránená");
            $this->redirect('this');
        }

        $this->redirect('this');
    }

    public function createComponentEventForm() {
        $eventForm = $this->eventFormFactory->create();

        $eventForm->setOwner($this->user);

        $eventForm->onValidationError[] = function($errors, $formErrors = []) {
            $allErrors = array_merge($errors, $formErrors);
            foreach ($allErrors as $errorMessage) {
                $this->flashMessage($errorMessage, 'error');
            }
        };

        $eventForm->onEventCreated[] = function($event) {

            $day = $event->start_time->format("d");
            $month = $event->start_time->format("m");
            $year = $event->start_time->format("Y");

            $this->flashMessage('Udalosť  bola úspešne vytvorená', 'success');
            $this->redirect('Calendar:day', ['day' => $day, 'month' => $month, 'year' => $year]);
        };

        $eventForm->onEventSaved[] = function($event) {
            $this->flashMessage('Udalosť  bola úspešne zmenená', 'success');
            $this->redirect('Calendar:edit', ['id' => $event->id]);
        };

        return $eventForm;
    }

    public function actionDay($day, $month, $year) {
        $this->day = $this->template->day = $day;
        $this->month = $this->template->month = $month;
        $this->year = $this->template->year = $year;
    }

    public function test() {
        $latte = new \Latte\Engine;

        $now = new \DateTime;
        $end = clone($now);
        $reminders = $this->eventManager->getReminders($now, $end->modify('+10 minutes'));


        //$output->write(sprintf("%d reminders", count($reminders)));


        $mail = new \Nette\Mail\Message();
        $mail->setFrom($this->from);
        //$users = $reminder->getUsers();
        /* foreach ($users as $user) {
          $mail->addTo($user->getEmail());
          } */
        $mail->setText('sdgdfhdfb');
        $mailer->send($mail);

        /* foreach ($reminders as $reminder) {
          $params = [
          'event' => $reminder->event_id,
          'link' => 'http://kalendar.hako.sk/calendar/show/'.$reminder->event_id->id
          ];
          $mail = new Message();
          $mail->setFrom($this->from);
          $users = $reminder->getUsers();
          foreach ($users as $user) {
          $mail->addTo($user->getEmail());
          }
          $mail->setHtmlBody($latte->renderToString(__DIR__ . '/templates/email.latte', $params));
          //dump($mail);
          $mailer->send($mail);
          } */
    }

}
