<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Presenters;

/**
 * Description of AdminPresenter
 *
 * @author vrabec
 */
class AdminPresenter extends BasePresenter{

    /** @var \Components\Factories\ICategoryAdministrationFactory  @inject*/
    public $categoryAdministrationFactory;
    
    /** @var \Components\Factories\IAddCategoryFormFactory @inject */
    public $addCategoryFormFactory;
    
    /** @var \App\Model\EventManager @inject*/
    public $eventManager;
    
    public function actionCategories(){

    }
    
    public function createComponentCategoryAdministration() {
        
        $categoryAdministration = $this->categoryAdministrationFactory->create();
        
        return $categoryAdministration;
        
    }
    
    public function createComponentAddCategoryForm(){
        
        $form = $this->addCategoryFormFactory->create();
        
        $form->onSuccess[] = function($success){
            $this->flashMessage($success);
            $this->redirect('this');
        };
        
        $form->onError[] = function($error){
            $this->flashMessage($error);
            $this->redirect('this');
        };
            
        return $form;
        
    }
    
    public function handleChangecategory($category, $name){
        
        $this->eventManager->updateCategory($category, $name);
        
    }
    
    
}
