<?php

namespace App\Presenters;

use Nette\Utils\Html;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;
use App\Model\EventTypeRecognizer;

class EventsPresenter extends \App\Presenters\BasePresenter {

    const FILTER_ALL = 'all';
    const FILTER_OVERDUE = 'overdue';
    const FILTER_VALID = 'valid';

    /** @var \Nette\Http\SessionSection */
    private $sessionSection;
    protected $filter;

    /** @var \Components\Factories\IEventsFilterFactory @inject */
    public $eventFilterFactory;

    /** @var \Components\Factories\IEventObjecFilterFactory @inject */
    public $eventFilterObjectsFactory;

    /** @var \Components\Factories\IBudgetEventFilterFactory @inject */
    public $budgetEventFilterFactory;

    /** @var \Components\Factories\IEventsSearchFormFactory @inject */
    public $eventsSearchFactory;

    /** @var \App\Model\EventManager @inject */
    public $eventManager;

    /** @var \App\Model\NoeticManager @inject */
    public $noeticManager;

    /** @var \App\Model\FVEManager @inject */
    public $fveManager;

    /** @var \App\Model\VehiclesManager @inject */
    public $vehicleManager;

    /** @var \App\Model\BuildingsManager @inject */
    public $buildingsManager;

    /** @var \App\Model\UsersManager @inject */
    public $userManager;

    //** @var \App\Model\EventTypes @inject */
    //public $eventTypes;

    /** @var string */
    public $search;

    public function startup() {
        parent::startup();
        $this->sessionSection = $this->session->getSection('all-events');
    }


    public function createComponentEventsFilter() {
        $filter = $this->eventFilterFactory->create();
        $filter->setSessionNamespace('all-events');

        return $filter;
    }

    public function createComponentEventsObjectFilter() {
        $filter = $this->eventFilterObjectsFactory->create();
        $filter->setSessionNamespace('all-events');

        return $filter;
    }

    public function createComponentBudgetEventsFilter() {
        $filter = $this->budgetEventFilterFactory->create();
        $filter->setSessionNamespace('all-events');

        return $filter;
    }

    public function createComponentEventsSearchForm() {

        $form = $this->eventsSearchFactory->create();

        $form->onResults[] = function($search) {
            $this->search = $search;
            if (!$this->isAjax()) {
                $this->redirect('this');
            } else {
                $this['grid']->redrawControl();
            }
        };

        return $form;
    }

    public function actionDefault($filter) {
        $params = $this['eventsObjectFilter']->params;
        $this['eventsObjectFilter']->setFilter($params);
        
        $this->filter = $filter;
    }

    public function createComponentGrid() {

        $grid = new \Grido\Grid();

        $searchIds = null;

        $grid->setTranslator(new \Grido\Translations\FileTranslator("sk"));

        $repository = $this->noetic_em->getRepository("App\Event");

        $from = $this['eventsFilter']->getFrom();
        $to = $this['eventsFilter']->getTo();

        $fromBudget = $this['budgetEventsFilter']->getFrom();
        $toBudget = $this['budgetEventsFilter']->getTo();

        $filter = $this['eventsObjectFilter']->getSelected();

        $query = $repository->createQueryBuilder('e')
                ->leftJoin('e.done_event','de')
        ;

        if (isset($from)) {
            $query->where('e.start_time >= :start')
                    ->setParameter('start', $from)
            ;
        }

        if (isset($to)) {
            $query->andWhere('e.start_time <= :end')
                    ->setParameter('end', $to)
            ;
        }

        if (isset($this->search)) {
            $results = $this->eventManager->search($this->search);

            $searchIds = array_map(function($results) {
                return $results['id'];
            }, $results);
        }

        if (!is_null($searchIds)) {
            $query->andWhere('e.id IN (:ids)');
            $query->setParameter('ids', $searchIds);
        }
        /*
         //status filter
        $status_filter = $this->filter;
        if ($status_filter == self::FILTER_VALID){
            $query->andWhere('e.done_event != :null')->setParameter('null', 'N;'); //not null
        }
        
        if ($status_filter == self::FILTER_OVERDUE){
            $query->andWhere('de IS NULL');
        }
        */
        if (isset($filter)) {
            switch ($filter['type']) {
                case BasePresenter::NOETIC_SELECTED: $query->andWhere('e.scada_id = :object');
                    break;
                case BasePresenter::FVE_SELECTED: $query->andWhere('e.fve_id = :object');
                    break;
                case BasePresenter::VEHICLE_SELECTED: $query->andWhere('e.vehicle = :object');
                    break;
                case BasePresenter::PEOPLE_SELECTED: $query->andWhere('e.person = :object');
                    break;
                case BasePresenter::BUILDING_SELECTED: $query->andWhere('e.building = :object');
                    break;
            }
            $query->setParameter('object', $filter['object']);
        }

        if (isset($filter['category'])) {
            $category = $this->eventManager->getCategoryById($filter['category']);
            $query->andWhere('e.category = :category')
                    ->setParameter('category', $category)
            ;
        }

        if (isset($fromBudget)) {
            $query->andWhere('e.budget >= :fromBudget')
                    ->setParameter('fromBudget', $fromBudget)
            ;
        }

        if (isset($toBudget)) {
            $query->andWhere('e.budget <= :toBudget')
                    ->setParameter('toBudget', $toBudget)
            ;
        }

        $model = new \Grido\DataSources\Doctrine($query);

        $grid->model = $model;

        $grid->addColumnText('id', 'ID');
        $grid->addColumnDate('start_time', 'Dátum');
        $grid->addColumnText('title', 'Názov');
        $grid->addColumnText('scada_id', 'Objekt')
                ->setCustomRender(function($item) {
                    $event = EventTypeRecognizer::recognizeEvent($item);
                    return $event->renderName();
                });

        $grid->addColumnText('done_event', 'Dokončená')
                ->setCustomRender(function($item) {
                    if (is_null($item->done_event)) {
                        $el = Html::el('span')->class('red done_event');
                        $el->setText('Nedokončená');
                        return $el;
                    } else {
                        $div = Html::el('div');
                        $el = Html::el('span')->class('green done_event');
                        $el->setText('Dokončená');
                        $div->add($el);
                        $div->add(Html::el('div')->class('small green')->setText($item->done_event->date->format('d.m.Y H:i:s')));
                        return $div->_toString();
                    }
                });

        $grid->addColumnText('budget', 'Rozpočet');

        $grid->addActionHref('show', 'Ukázať')->setCustomRender(function($item) {
            if (is_null($item->done_event)) {
                $html = Html::el('a', [
                            'href' => $this->link('Calendar:show', $id = $item->id),
                            'class' => 'fas fa-eye'
                ]);

                return $html;
            }
        });

        $grid->addActionHref('edit', 'Edit')->setCustomRender(function($item) {
            if (!$item->done_event) {
                $html = Html::el('a', [
                            'href' => $this->link('Calendar:edit', $id = $item->id),
                            'class' => 'fas fa-pencil-alt'
                ]);

                return $html;
            }
        });

        $grid->addActionHref('success', 'Success')->setCustomRender(function($item) {
            if (is_null($item->done_event)) {
                $html = Html::el('a', [
                            'href' => $this->link('confirmEvent!', $id = $item->id),
                            'onclick' => 'return confirm("Naozaj chcete označiť udalosť za vykonanú")',
                            'class' => 'fas fa-check'
                ]);

                return $html;
            }
        });

        $grid->addActionHref('remove', 'Remove')->setCustomRender(function($item) {
            if (is_null($item->done_event)) {
                $html = Html::el('a', [
                            'href' => $this->link('removeEvent!', $id = $item->id),
                            'onclick' => 'return confirm("Naozaj chcete odstrániť túto udalosť?")',
                            'class' => 'fa fa-trash'
                ]);

                return $html;
            }
        });

        $grid->setDefaultSort(['start_time' => 'ASC']);

        return $grid;
    }

    private function setupFilter(){    
        if (is_null($this->filter)){
            if (is_null($this->sessionSection->offsetGet('filter'))){
                $this->filter = self::FILTER_ALL;
                $this->sessionSection->offsetSet('filter', $this->filter);        
            }
            
            $this->filter = $this->sessionSection->offsetGet('filter');
        } else {
            $this->filter = $this->sessionSection->offsetSet('filter', $this->filter);
        }
    }

    public function handleRemoveEvent($id) {

        $event = $this->eventManager->findById($id);

        if (!$event) {
            $this->redirect('Calendar:day');
        }

        if (!$this->user) {
            $this->flashMessage('Používateľ musí byť prihlásený');
            $this->redirect('this');
        }

        try {
            $this->eventManager->removeEvent($event);
        } catch (Exception $e) {
            $this->flashMessage($e);
            $this->redirect('this');
        } finally {
            $this->flashMessage("Udalosť bola odstránená");
            $this->redirect('this');
        }

        $this->redirect('this');
    }

    /**
     * Flag event asit is done
     * @param type $id
     */
    public function handleConfirmEvent($id) {

        //find event
        $event = $this->eventManager->findById($id);

        if (!$event) {
            $this->redirect('Calendar:day');
        }

        if (!$this->user) {
            $this->flashMessage('Používateľ musí byť prihlásený');
            $this->redirect('this');
        }

        $now = new \DateTime;

        $user = $this->userManager->getUser($this->user->id);

        $done_event = new \App\DoneEvents();
        $done_event->event = $event;
        $done_event->date = $now;
        $done_event->user = $user;

        $this->eventManager->commitEvent($done_event);

        $this->flashMessage("Udalosť bola dokončená", "success");
        $this->redirect('this');
    }

    private function getMinMaxBudget() {
        $minMax = $this->eventManager->getMinMaxBudget();

        return $minMax;
    }
    
    public function renderDefault(){
        $this->setupFilter();
        
        $this->template->filter = $this->filter;
        
        //$this->template->render();
        
    }

}
