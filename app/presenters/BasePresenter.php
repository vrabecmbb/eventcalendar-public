<?php

namespace App\Presenters;

use Nette;
use App\Model;
use Kdyby\Doctrine\Registry;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter {

    const FVE_SELECTED = 'fve';
    const NOETIC_SELECTED = 'noetic';
    const PEOPLE_SELECTED = 'people';
    const VEHICLE_SELECTED = 'vehicle';
    const BUILDING_SELECTED = 'building';

    /* @var Doctrine\ORM\EntityManager @inject */

    public $em;
    public $noetic_em;
    public $solar_em;

    public function __construct(Registry $registry) {
        parent::__construct();
        $this->noetic_em = $registry->getManager('noetic');
        $this->solar_em = $registry->getManager('solar');
    }

    public function beforeRender() {
        if ($this->user->isLoggedIn()) {
            $this->template->userData = $this->user->getIdentity()->getData();
        }
    }

    public function handleLogout() {
        
        if ($this->user) {
            $this->user->logout(TRUE);
        }
        $this->redirect('this');
    }

    protected function recognizeObject($event){
        if ($event->scada_id){
            return $event->scada_id->name;
        }
        if ($event->fve_id){
            return $event->fve_id->name;
        }
        if ($event->person){
            return $event->person->name;
        }
        if ($event->vehicle){
            return $event->vehicle->id;
        }
        if ($event->building){
            return $event->building->name;
        }
    }
    
    protected function recognizeObjectType($event){
        if ($event->scada_id){
            return self::NOETIC_SELECTED;
        }
        if ($event->fve_id){
            return self::FVE_SELECTED;
        }
        if ($event->person){
            return self::PEOPLE_SELECTED;
        }
        if ($event->vehicle){
            return self::VEHICLE_SELECTED;
        }
        if ($event->building){
            return self::BUILDING_SELECTED;
        }
    }
    
    protected function getObjectIcon($event){
        if ($event->scada_id){
            return 'fas fa-sitemap';
        }
        if ($event->fve_id){
            return 'fas fa-certificate';
        }
        if ($event->person){
            return 'fas fa-user';
        }
        if ($event->vehicle){
            return 'fas fa-car';
        }
        if ($event->building){
            return 'fas fa-building';
        }
    }
}
