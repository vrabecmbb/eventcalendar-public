<?php

namespace App\Presenters;

use Nette;
use App\Model;
use App\Model\EventTypeRecognizer;

class HomepagePresenter extends BasePresenter {

    /** @var \Components\Factories\ICalendarFactory @inject */
    public $calendarFactory;

    /** @var Model\EventManager @inject */
    public $eventManager;
    public $request;

    public function actionLogin($request) {
        $this->request = $request;
    }

    public function createComponentCalendar() {
        $calendar = $this->calendarFactory->create();

        $calendar->setCalendarGenerator(new \CalendarGenerator(new \IHorizontalCalendarCellFactory(1)));

        return $calendar;
    }

    public function createComponentLoginForm() {
        $form = new Nette\Application\UI\Form();

        $form->addText('login', 'Login:')
                ->setRequired()
                ->setAttribute('class', 'input-xlarge');
        $form->addPassword('heslo', 'Heslo:')
                ->setRequired()
                ->setAttribute('class', 'input-xlarge');
        $form->addSubmit('ok', 'Prihlásenie');

        $form->onSuccess[] = [$this, 'processLogin'];

        return $form;
    }

    /**
     * Login 
     * @param Nette\Application\UI\Form $form
     */
    public function processLogin(Nette\Application\UI\Form $form) {
        $login = $form->getValues()['login'];
        $password = $form->getValues()['heslo'];

        try {
            $this->user->login($login, $password);

            $normRequest = \Nette\Utils\Json::decode($this->request, TRUE);
            if ($this->request) {
                $this->redirect($normRequest['presenter'] . ':' . $normRequest['action']['action'], $normRequest['action']);
            }
            $this->redirect('Homepage:default');
        } catch (\Nette\Security\AuthenticationException $ex) {
            $form->addError($ex->getMessage());
        }
    }

    /**
     * logout user
     */
    public function handleLogout() {
        $this->user->logout(TRUE);
        $this->redirect(':Homepage:default');
    }

    public function actionAgenda() {
        $now = new \DateTime;
        $date = clone($now);
        $addMonth = $date->modify('+1 month')->setTime(23, 59, 59);


        $expired = $this->eventManager->getExpiredEvents($now);
        $nextMonth = $this->eventManager->getLimitedEvents($now, $addMonth);

        $this->template->expired = $this->prepareAgenda($expired);
        $this->template->agenda = $this->prepareAgenda($nextMonth);
    }

    private function prepareItemObject($item) {
       
        $event = EventTypeRecognizer::recognizeEvent($item);
        
        $id = $item->id;
        $name = $event->getName();
        $icon = $event->getIcon();
        $event_name = $item->title;
        $start_time = clone($item->start_time);

        $json_object = new \stdClass();
        $json_object->start_time = $start_time;
        $json_object->icon = $icon;
        $json_object->object_name = $name;
        $json_object->event_name = $event_name;
        $json_object->id = $id;

        return $json_object;
    }

    /**
     * Prepare list of agenda
     * @param array $agendaList
     * @return type
     */
    private function prepareAgenda(array $agendaList) {

        if (!empty($agendaList)) {

            $toRet = [];

            foreach ($agendaList as $item) {
                $humanized_item = $this->prepareItemObject($item);
                $toRet[] = $humanized_item;
            }
            return $toRet;
        }
        return [];
    }

}
