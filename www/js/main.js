$(function () {

    //diary search
    var xhr = null;
    var $spinner = $('.spinner');
    
    $spinner.hide();
    $('.form-search ').on('keyup', function () {
        var inputSearch = $(this).find($('input[name="search"]'));

        var searchVal = inputSearch.val();

        var link = inputSearch.data('link').replace('__REPLACEME__', searchVal);
        //link = link.replace('__REPLACETYPE__', searchTypeVal);

        if (searchVal.length >= 0) {
            $spinner.show();
            if (xhr !== null) {
                xhr.abort();
            }
            xhr = $.post(link, function (payload) {
                $spinner.hide();
                jQuery.nette.success(payload);
            });
        }
    });
});
