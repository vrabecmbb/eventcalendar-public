$(function () {
    var t = this;

    //ak je form vyplneny
    if ($('#form_event_reminder').is(':checked')) {
        $('#reminder_content').removeClass('d-none');
    } else {
        $('#reminder_content').addClass('d-none');
    }

    if ($('#form_event_repeater').is(':checked')) {
        $('#repeater_content').removeClass('d-none');
    } else {
        $('#repeater_content').addClass('d-none');
    }

    //activate pill
    var id = $('#object_id').val();

    if ($('#object_id').val() !== "") {

        $("#pills-eleobject").find(".tab-pane").removeClass('active');

        if ($('#object_type').val() === "noetic") {
            $("ul#pills-objectype-tab").find('a').removeClass('active');
            $("#pills-noetic-tab").addClass('active');
            $("#pills-noetic").addClass('show active');
        } else if ($('#object_type').val() === "fve") {
            $("ul#pills-objectype-tab").find('a').removeClass('active');
            $("#pills-fve-tab").addClass('active');
            $("#pills-fve").addClass('show active');
        } else if ($('#object_type').val() === "vehicle") {
            $("ul#pills-objectype-tab").find('a').removeClass('active');
            $("#pills-vehicles-tab").addClass('active');
            $("#pills-vehicles").addClass('show active');
        } else if ($('#object_type').val() === "building") {
            $("ul#pills-objectype-tab").find('a').removeClass('active');
            $("#pills-buildings-tab").addClass('active');
            $("#pills-buildings").addClass('show active');
        } else if ($('#object_type').val() === "people") {
            $("ul#pills-objectype-tab").find('a').removeClass('active');
            $("#pills-users-tab").addClass('active');
            $("#pills-users").addClass('show active');
        }

        //activate selected pill
        console.log($('#pills-eleobject').find('.active'));
        console.log(id);
        $('#pills-eleobject').find('.active').find(`[data-id='` + id + `']`).addClass('active');
    }

    //activate pills on reminder users
    if ($('#reminder_mails').val() !== "") {
        $users = $('#reminder_mails').val();

        var array = JSON.parse("[" + $users + "]");

        var mail_chooser = $(".event_mail_chooser");

        array.forEach(function (id) {
            mail_chooser.find(`[data-id='` + id + `']`).addClass('active');
        });
    }
    
    
    
    var loadOptions = function (object_type, selected) {
        $.ajax({
            url: "?eventForm-object_type=" + object_type + "&do=eventForm-getCategoryOptions"
        }).done(function (result) {
            var ret = "";
            var parsed = $.parseJSON(result);

            parsed.forEach(function (item) {
                console.log(item['id'],selected, item['id'] === selected);
                ret += "<option value='" + item['id'] + "'" + (item['id'] === selected ? " selected" : "") + ">" + item['name'] + "</option>";
            });
            

            $('#object_category').html(ret);

            $("#category_box").removeClass('d-none');
        });
    };
    
    $.ajax({
            url: "?&do=eventForm-getSelectedType"
        }).done(function (result) {
            var result = JSON.parse(result);
            var type = result.type;
            var selectedCategory = parseInt($("#object_category_hidden").val());
            
            loadOptions(type, selectedCategory);
            
        });
        
    
    //loadOptions('noetic', parseInt(t.selectedCategory));

    $('#pills-objectype-tab .nav-item').click(function (e) {
        //category select when object choosed
        var type = $(e.target).attr("type");
        loadOptions(type);
        /*$.ajax({
         url: "?eventForm-object_type=" + type + "&do=eventForm-getCategoryOptions"
         }).done(function (result) {
         var ret = "";
         var parsed = $.parseJSON(result);
         
         parsed.forEach(function (item) {
         ret += "<option value='" + item['id'] + "'>" + item['name'] + "</option>";
         });
         
         $('#object_category').html(ret);
         
         $("#category_box").removeClass('d-none');
         });*/

    });

    $("#object_category").change(function (e) {
        var value = $(this).val();
        $("#object_category_hidden").val(value);
    });


    $("ul.ele_obj_changer li").click(function () {
        var type = $(this).attr('object-type');
        var obj = $(this).attr('data-id');

        $('#object_type').val(type);
        $('#object_id').val(obj);

        $("ul.ele_obj_changer li").removeClass('active');
        $(this).addClass('active');

    });

    var choosedUsers = [];

    $("ul.event_mail_chooser li").click(function () {
        var obj = $(this).attr('data-id');

        $('#reminder_mail').val(obj);

        //$("ul.ele_obj_changer li").removeClass('active');
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            var index = choosedUsers.indexOf(obj);
            if (index !== -1)
                choosedUsers.splice(index, 1);
        } else {
            $(this).addClass('active');
            choosedUsers.push(obj);
        }

        $('#reminder_mails').val(choosedUsers);
    });

    //reminder content
    $('#form_event_reminder').change(function () {

        if ($(this).is(":checked")) {
            $('#reminder_content').removeClass('d-none');
        } else {
            $('#reminder_content').addClass('d-none');
        }
    });

    //repeater content
    $('#form_event_repeater').change(function () {

        if ($(this).is(":checked")) {
            $('#repeater_content').removeClass('d-none');
        } else {
            $('#repeater_content').addClass('d-none');
        }
    });

});