<?php

// Uncomment this line if you must temporarily take down your site for maintenance.
// require __DIR__ . '/.maintenance.php';

$configurator = require __DIR__ . '/../app/bootstrap.php';

$container = $configurator->createContainer();

if (PHP_SAPI != 'cli')
{
    $container->getService('application')->run();
}
